﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoBota : MonoBehaviour
{// float es un número decimal
    public float velocidad = 25;   //le damos un valor por defecto
    GameObject jugador;
    

 
    // Start se le llama la primera vez, primer frame
    private void Start()
    {
        // Le decimos que la posición de inicio en eje x sea al azar entre (-10 y 10)
        float posInicioX = Random.Range(-10, 10);
            this.transform.position = new Vector3(posInicioX, 12, 1);


    // Buscamos el objeto de juego JugadorCaballito para poder crear las "colisiones" entre ellos
        jugador = GameObject.Find("Jugador-Caballito"); //Esta operación NUNCA SE DEBE HACER EN EL UPDATE

    }
    // Update is called once per frame
    void Update()
    {
        // Le decimos cómo tiene que moverse
        Vector3 movAbajo = velocidad *
            new Vector3(0, -1, 0) * Time.deltaTime;
        this.GetComponent<Transform>().position = this.GetComponent<Transform>().position + movAbajo;

        // Ahora le decimos que detecte en cada frame si el personaje coincide con la lata en el eje x
        if (this.GetComponent<Transform>().position.y < 0)
        {           
            this.GetComponent<Transform>().position = new Vector3(this.transform.position.x, 0, 1);

            // Determinamos los puntos de colisión entre el enemigo y el personaje
           if (this.transform.position.x >= jugador.transform.position.x - 3.2f/2
            && this.transform.position.x <= jugador.transform.position.x + 3.2f/2)
           
           { // Se detruye si pasa una acción ya determinada por el controlador de juego
                GameObject.Find("Controlador-Juego").GetComponent<ControladorJuego>().CuandoCapturamosEnemigoBota();
                Destroy(this.gameObject);                
            } 
            else 
             {
                GameObject.Find("Controlador-Juego").GetComponent<ControladorJuego>().CuandoPerdemosEnemigoBota();
                Destroy(this.gameObject);
                
              }

        }

    }    
  
}
