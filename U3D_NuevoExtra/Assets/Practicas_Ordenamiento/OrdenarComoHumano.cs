﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace AlgoritmosOrdenamientos
{
    public class OrdenarComoHumano
    {
        public static void Main()
        {
            //int listaOrdenados;
            
            int menor = 0;
            List<int> numeros = new List<int>();
            numeros.Add(4);
            numeros.Add(5);
            numeros.Add(2);
            numeros.Add(3);
            numeros.Add(1);

            menor = BuscarMenor(numeros);
            Debug.Log(menor);
            MostrarLista(numeros);
            List<int> numOrdenados = new List<int>();
           
            // Invocar al método ordenar y mostrar
            numOrdenados = Ordenar(numeros);
            MostrarLista(numOrdenados);           
           
        }
        

        public static List<int> Ordenar(List<int> lista)
        {
            
          //TODO: Ordenar, buscando el más pequeño, sacándolo y poniendo la lista ordenada
          List<int> listaOrdenados = new List<int>();
          int menor = 0;
           // Console.WriteLine("Inicio lista ordenada");
            int recorridoInicial = lista.Count;
            for (int i = 0; i < recorridoInicial; i++)
            {               
             menor = BuscarMenor(lista);
             listaOrdenados.Add(menor);
             lista.Remove(menor);        
            }
            MostrarLista(listaOrdenados);
            return listaOrdenados;            
        }

        public static void MostrarLista(List<int> lista)
        {
            Debug.Log("Lista: ");
            for (int i = 0; i < lista.Count; i++)
            {
                Debug.Log(lista[i]);
            }
            Debug.Log("  FIN");
        }
        //TODO: Para usar la función estática, falta algo
        public static int BuscarMenor(List<int> lista)
        {
            
            int menor = lista[0];
            for (int i = 0; i < lista.Count; i++)
            {
                if (lista[i] < menor)
                {
                    menor = lista[i];                    
                }                          
            }
            //Console.WriteLine("El número menor de la lista es: " + menor);
            return menor;
            
        }
    }
}
