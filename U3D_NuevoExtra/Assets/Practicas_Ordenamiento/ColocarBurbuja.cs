﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ColocarBurbuja : MonoBehaviour
{
    public List<Text> mandarinas;
    List<int> listaMandarinas;
    int posicionMenor;
    // Start is called before the first frame update
    void Start()
    {
        listaMandarinas = new List<int>();
        foreach (Text txtMandarinas in mandarinas)
        {
            int num = Int32.Parse(txtMandarinas.text);
            listaMandarinas.Add(num);
            
        }
        // OrdenarBurbuja.Ordenacion(listaMandarinas); 
       // OrdenarBurbuja.OrdenarPorExtraccion(listaMandarinas);
        OrdenarBurbuja.OrdenarPorSeleccion(listaMandarinas);
    }
    



}
