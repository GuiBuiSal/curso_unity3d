﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrdenarBurbuja : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        List<int> burbuja = new List<int>();
        burbuja.Add(5);
        burbuja.Add(3);
        burbuja.Add(2);
        burbuja.Add(1);
        burbuja.Add(4);
       
       
        //Ordenacion(burbuja);
        //OrdenarPorExtraccion(burbuja);
        OrdenarPorSeleccion(burbuja);
    }

    public static void Ordenacion(List<int> burbuja)
    {
               // Debug.Log("Comienzo del bucle: " + "Valor [0] " + burbuja[0] + "" + "Valor [1] " + burbuja[1] + "" + "Valor [2] " + burbuja[2] + "" + "Valor [3] " + burbuja[3] + "" + "Valor [4] " + burbuja[4] + " Fin de la cita");

        for (int i = 0; i < burbuja.Count; i++)
        {            
            for(int j = i + 1; j <= burbuja.Count-1; j++)
            {
                
                if (burbuja[i] >= burbuja[j])
                {                    
                    
                    Debug.Log("Pasada " + i);
                   int  varAux = burbuja[j];
                   int varAux2 = burbuja[i];
                     
                   // Debug.Log("Valor varAux2" + varAux2); = 3
                   burbuja[j] = varAux2;
                   burbuja[i] = varAux;
                   //Debug.Log("Valor [i]" + burbuja[i]);
                   // Debug.Log("Valor [j]" + burbuja[j]);             
                    
                }
            }
            MostrarLista(burbuja);
        }                      
    }
    public static void OrdenarPorExtraccion(List<int> burbuja)
    {
        for(int i = 0; i < burbuja.Count; i++)
        {
            for (int j = i + 1; j <= burbuja.Count - 1; j++) 
            {
                if (burbuja[i] >= burbuja[j])
                {
                    int varAux = burbuja[j];
                    burbuja.RemoveAt(j);
                    burbuja.Insert(i, varAux);
                    
                }
            }
            MostrarLista(burbuja);
        }
    }

    public static void OrdenarPorSeleccion(List<int> burbuja) 
    {
        
        for (int i = 0; i< burbuja.Count; i++)
        {
            int numMinimo = 0;
            for (int j = 0; j < burbuja.Count; j++)
            {
                if (burbuja[i] < burbuja.Count)
                {
                    
                   numMinimo = burbuja[j];
                    Debug.Log("El mínimo es " + numMinimo + " la posición del mínimo es " + i);                  
                    
                }

                /*burbuja[j] = burbuja[i];
                Debug.Log("Después de la asignación: " + burbuja[j]);
                numMinimo = burbuja[i];
                Debug.Log("Después del intercambio: " + burbuja[i]);*/

            }
            MostrarLista(burbuja);
           
           
        }
        
    }

    public static void MostrarLista(List<int> burbuja)
    {
        Debug.Log("Lista: ");
        for (int i = 0; i < burbuja.Count; i++)
        {
            Debug.Log(burbuja[i]);
        }
        Debug.Log("  FIN");
    }

}
