﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using AlgoritmosOrdenamientos;


public class ColocarNumUI : MonoBehaviour
{
    
    public List<Text> numeros;
    List<int> listaNum;
    int menorDeTodos;
    public Vector2 posInicial;
    const int ANCHO_TEXTO = 100;

    // Start is called before the first frame update
    void Start()
    {
        ColocarSeguidos();
        listaNum = new List<int>();
        foreach (Text txtNum in numeros) 
        {
            int num = Int32.Parse(txtNum.text);
            listaNum.Add(num);
        }
        menorDeTodos = OrdenarComoHumano.BuscarMenor(listaNum);
        
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    public void ColocarSeguidos()
    {
        this.posInicial = this.numeros[0].GetComponent<RectTransform>().localPosition;

       /* Si es constante la separación
        for (int i = 1; i < this.numeros.Count; i++)
        {
            float nuevaPosX = this.numeros[i - 1].rectTransform.localPosition.x + ANCHO_TEXTO;
            float nuevaPosY = this.posInicial.y;
            Vector2 nuevaPos = new Vector2(nuevaPosX, nuevaPosY);
            this.numeros[i].rectTransform.localPosition = nuevaPos;
        }*/

        // La sepàración es el ancho de cada caja, unida a la siguiente
        for (int i = 1; i < this.numeros.Count; i++)
        {
            float nuevaPosX = this.numeros[i - 1].rectTransform.localPosition.x + this.numeros[i-1].rectTransform.rect.width / 2 + this.numeros[i].rectTransform.rect.width/2;
            float nuevaPosY = this.posInicial.y;
            Vector2 nuevaPos = new Vector2(nuevaPosX, nuevaPosY);
            this.numeros[i].rectTransform.localPosition = nuevaPos;
        }


        /*List<int> listaPosX = new List<int>();
        foreach (Text txtNum in this.numeros)
        {
            int posX = (int)txtNum.rectTransform.localPosition.x;
            listaPosX.Add(posX);
        
        }
        int posMenorX = OrdenarComoHumano.BuscarMenor(listaPosX);
        this.posInicial = new Vector2(posMenorX, this.numeros[0].rectTransform.rect.position.y);

        this.numeros[0].rectTransform.localPosition = this.posInicial;*/
    }
}
