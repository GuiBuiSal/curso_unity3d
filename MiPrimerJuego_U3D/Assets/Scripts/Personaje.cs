﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour
{
    // float es un numero decimal
    public float velocidad = 40;    // le damos un valor concreto // 40 es el valor por defecto
                                    

    GameObject imagenCaballo;

    // Start se le llama la primera vez, primer frame
    private void Start()
    {
        // Buscamos un GameObject
        imagenCaballo = GameObject.Find("Sprite-Caballito"); //Esta operación NUNCA SE DEBE HACER EN EL UPDATE
     
        
    }


    // Update is called once per frame    
    void Update()
    {
        // Cuando se pulsa <--

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            // Creamos el vector de movimiento, a partir del cálculo que
            // influye la velocidad (40), el vector hacia la izquierda (-1,0,0):       (-40,0,0)
            // Para que se mueva -40 unidades por segundo en vez de por cada frame,
            // multiplicamos por el incremento del tiempo (aprox. 0,02 seg, 50 FPS)  (0,8) (0,0)

            Vector3 vectorMov = velocidad * Vector3.left * Time.deltaTime;
            this.GetComponent<Transform>().Translate(vectorMov);

            // Si la posición en el eje x es menor de -10 en el margen izquierdo
            if (this.GetComponent<Transform>().position.x < -10)
            {
        // Entonces recolocamos en el margen izquierdo
                this.GetComponent<Transform>().position = new Vector3(-10, 0, 0);
                Debug.Log("Choque a la izquierda: ");
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                imagenCaballo.GetComponent<SpriteRenderer>().flipX = false;
            }

        }

        // Cuando se pulsa -->

        if (Input.GetKey(KeyCode.RightArrow))

           
        {
           

            Vector3 vectorMov = velocidad * Vector3.right * Time.deltaTime;
            this.GetComponent<Transform>().Translate(vectorMov);

            


            // Si la posición en el eje x es mayor de 10 en el margen derecho
            if (this.GetComponent<Transform>().position.x > 10)
            {
                // Entonces recolocamos en el margen derecho
                this.GetComponent<Transform>().position = new Vector3(10, 0, 0);
                Debug.Log("Choque a la derecha: ");
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                imagenCaballo.GetComponent<SpriteRenderer>().flipX = true;
            }

        }
    }
}
