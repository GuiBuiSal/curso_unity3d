﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorJuego : MonoBehaviour
{

    public GameObject[] enemigos;
    

    public float tiempo = 0.0f;
    public int vidas = 12;
    public int puntos = 0;

    /*public GameObject prefabEnemigoLata;
    public GameObject prefabEnemigoBota;
    public GameObject prefabEnemigoBotella;
    */
    public GameObject textoVidas;
    public GameObject textoPuntos;
    public GameObject textoTiempo;
   
    // Start is called before the first frame update
    void Start()
    {
       this.InstanciarEnemigo();        
    }
    
    // Update is called once per frame
    void Update()
    {
        this.textoVidas.GetComponent<Text>().text = "Vidas: " + this.vidas;
        this.textoPuntos.GetComponent<Text>().text = "Puntos: " + this.puntos;
        this.textoTiempo.GetComponent<Text>().text = "Time " + this.tiempo.ToString("f0");
        tiempo -= Time.deltaTime;

        if (vidas < 1)
        { this.textoVidas.GetComponent<Text>().text = "Vidas: " + 0; }

        if (tiempo < 1) 
        { this.textoTiempo.GetComponent<Text>().text = "Time " + 0;}

        // Si los puntos actuales son distintos al 
        //.....
        //if (this.puntos != puntosAnt)
        //{
        //  this.InstanciarEnemigo();
        //}
        //this.puntosAnt = this.puntos;
    }


    // Esto es un nuevo método (acción, conjunto de intrucciones, función, 
    // procedimiento, mensaje...)
    public void CuandoCapturamosEnemigo()
    {
        // Asignando un nuevo valor que es  los puntos actuales +1 punto
        // this.puntos +=1

        if (vidas > 0 && tiempo > 0)
        {
            this.InstanciarEnemigo(); 
        }

        if (vidas > 0 && tiempo > 0)  puntos += 1;  else puntos += 0; 
        
    }

    public void CuandoPerdemosEnemigo()
    {
     vidas -= 1 ; // this.vidas -=1; this.vidas--
        
        if (vidas > 0 && tiempo > 0)
        {
            this.InstanciarEnemigo();
        }

    }
    public void InstanciarEnemigo()
    {
        int numEnemigo = Random.Range(0, enemigos.Length);
        if (numEnemigo == 0)    // 33 %
        {
            GameObject.Instantiate(enemigos[0]);
        }
        else if (numEnemigo == 1)   // 33 % 
        {
            GameObject.Instantiate(enemigos[1]);
        }
        else if (numEnemigo == 2)    // 33 %
        {
            GameObject.Instantiate(enemigos[2]);
        }
        else if (numEnemigo == 3)    // 33 %
        {
            GameObject.Instantiate(enemigos[3]);
        }

    }
}
    



