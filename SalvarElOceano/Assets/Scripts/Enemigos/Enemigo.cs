﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    public ControladorJuego controlJuego;
    public float velocidad;
    public int numEnemigo = 0;
    AudioSource audioCogerLata;
    public AudioClip audioChocarSuelo;
    // Dos objetos/componentes a enlazar
    //private Controlador controladorJuego;
    private GameObject jugador;
    

    // Start is called before the first frame update
    void Start()
    {
        jugador = GameObject.Find("Jugador");
        audioCogerLata = GameObject.Find("coger_lata").GetComponent < AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movAbajo = velocidad * new Vector3(0, -1, 0) * Time.deltaTime;

        this.GetComponent<Transform>().position += movAbajo;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (this.enabled)
        {
            Debug.Log("Enemigo colisionado con " + collision.gameObject.name);
            if (collision.gameObject.name.ToLower().Contains("final"))
            {
                
                this.enabled = false;

                this.GetComponent<Animator>().speed = 0;
                controlJuego.RestarVida(1);
                AudioSource.PlayClipAtPoint(audioChocarSuelo, Vector3.zero);
            }
            else if (collision.gameObject.name.ToLower().Contains("cuerpo") || collision.gameObject.name.ToLower().Contains("cabeza") || collision.gameObject.name.ToLower().Contains("cola"))
            {
                audioCogerLata.Play();
                Destroy(this.gameObject);
                controlJuego.SumarPuntos(100, this.numEnemigo);
            }
        }        
    }
   
}
