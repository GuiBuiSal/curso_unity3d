﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GuillermoSEO
{
    public class ControladorEscenas : MonoBehaviour
    {
        /* [Header("Propiedades control de escenas: ")]
         public string nombreEscena;

         void Imprimir(int x)
         {
             Debug.Log(x);
             Imprimir(x - 1);
         }

         private void Start()
         {
             if (nombreEscena != "")
             {
                 this.CargarEscena(nombreEscena);
             }
             AsignarCerrarAlBotonSalir();
         }

         // Update is called once per frame
         void Update()
         {
             if (Input.GetKey(KeyCode.Escape))
             {
                 Application.Quit();
             }
         }
         public void CargarEscena(string escena)
         {
             SceneManager.LoadScene(escena);
         }
         public void CerrarAplicacion()
         {

         }
         void AsignarCerrarAlBotonSalir()
         {
             Transform canvas = GameObject.Find("Canvas").GetComponent<Transform>();
             Transform botonSalirSi = FindByNameInactives(canvas, "Boton_Salir_Si");
         }
         Transform FindByNameInactives(GameObject raiz, string nombre)
         {
             for (int i = 0; i < raiz.getChildCount; i++)
             {
                 Transform objTransf = raiz.GetChild(i);
                 if(objTransf.name == nombre)
             }

         }*/

       
        
            [Header("Propiedades ctrl escenas:")]
            public string nombreEscena;
        // Ejemplo de recursividad
            void Imprimir(int x)
            {
                if (x > 0)
                {
                    Debug.Log("Entrando: " + x);
                    Imprimir(x - 1);
                }
                Debug.Log("Saliendo: " + x);
            }

            private void Start()
            {
                Imprimir(5);
                if (nombreEscena != "")
                {
                    print("Arrancando escena " + nombreEscena);
                    this.CargarEscena(nombreEscena);
                }
                
                AsignarCerrarAlBotonSalir();
            }
            // Update is called once per frame
            void Update()
            {
                if (Input.GetKey(KeyCode.Escape))
                {
                    CerrarAplicacion();
                }
            }
            public void CargarEscena(string escena)
            {
                SceneManager.LoadScene(escena);
            }
            // Vamos a centrarlizar aquí la funcionalidad de cuando se cierre la aplicación
            public void CerrarAplicacion()
            {
                Application.Quit();
            }
            void AsignarCerrarAlBotonSalir()
            {
                GameObject objCanvas = GameObject.Find("Canvas");
                if(objCanvas!= null)
                {
                    Transform canvas = GameObject.Find("Canvas").GetComponent<Transform>();
                    Transform botonSiSalirSi = null;
                    FindByNameInactives(canvas, "BotonSiSalirSi", ref botonSiSalirSi);
                    
                    if (botonSiSalirSi != null)
                    {
                        botonSiSalirSi.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(this.CerrarAplicacion);
                    }               
                    
                }                 
                    
            }
            void FindByNameInactives(Transform raiz, string nombre, ref Transform objetoEncontrado)
            {
                
                 for (int i = 0; i < raiz.childCount; i++)
                 {
                    Transform objTransf = raiz.GetChild(i);
                    if (objTransf.name == nombre)
                    {

                    objetoEncontrado = objTransf;
                    return;

                    }
                    else 
                    {
                     FindByNameInactives(objTransf, nombre, ref objetoEncontrado);
                     
                    }
                 }
                 
            }
        
    }

}

