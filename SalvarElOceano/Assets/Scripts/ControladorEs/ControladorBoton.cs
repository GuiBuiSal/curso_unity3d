﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorBoton : MonoBehaviour
{
    public int direccion = 0;    
    public GameObject jug;
    private bool pulsando;

   
    // Update is called once per frame
    void Update()
    {
        if (pulsando) 
        { 
            jug.GetComponent<MovimientoJugador>().Mover(direccion);
            if (direccion < 0)
            {
                jug.GetComponent<AnimacionJugador>().HaciaLaIzquierda();
            }
            if (direccion > 0) 
            { 
                jug.GetComponent<AnimacionJugador>().HaciaLaDerecha();
            }
           
        }
        
    }
    private void OnMouseDown()
    {
        pulsando = true;
    }
    
    private void OnMouseUp()
    {
        pulsando = false;   
    }
}
