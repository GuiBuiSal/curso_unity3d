﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorCamara : MonoBehaviour
{
    public GameObject jugador;
    public Transform Esq_MaxCam_Izq;
    public Transform Esq_MaxCam_Der;


    // Start is called before the first frame update
    void Start()
    {
        // Find cuesta trabajo. Sólo usarlo una vez por objeto y script. No USAR en el UPDATE.
        jugador = GameObject.Find("Jugador");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 posCam = this.transform.position;

        float x = Mathf.Lerp(this.transform.position.x, jugador.transform.position.x, 0.05f);
        // x = this.transform.position.x + (jugador.transform.position.x -this.transform.position.x) * 5%

        posCam.x = x;

        if (posCam.x < Esq_MaxCam_Izq.position.x)
        {
            posCam.x = Esq_MaxCam_Izq.position.x;

        }

        else if (posCam.x > Esq_MaxCam_Der.position.x)
        {
            posCam.x = Esq_MaxCam_Der.position.x;

        }       

        this.transform.position = posCam;
    }
}
