﻿using UnityEngine;
using System.Collections;
using UnityEngine.UIElements;

public class ControladorJuego: MonoBehaviour
{
    [Header("Prefab y objetos de juego")]
    // array prefab enemigos
    public GameObject[] prefabsEnemigos;

    //public GameObject prefabEnemigo;
    public GameObject esqSuperior;

    [Header("Objetos UI")]
    public int vidas = 7;
   
    public GameObject panelFinDeJuego;
    public GameObject panelGanador;
    public GameObject panelPerdedor;
    public UnityEngine.UI.Text textoVidas;
    public UnityEngine.UI.Text textoPuntos;

    [Header("Listado de enemigos")]
    public AparicionEnemigo[] apariciones;
    int puntos = 0;
    int enemigoActual;
    float timeInicio;
    

    // Use this for initialization
    void Start()
    {
        textoPuntos.text = "" + this.puntos;
        textoVidas.text = "" + this.vidas;

        /*this.apariciones = new AparicionEnemigo[3];
        this.apariciones[0] = new AparicionEnemigo(2,1);
        this.apariciones[1] = new AparicionEnemigo(-3,6);
        this.apariciones[2] = new AparicionEnemigo(7,10);*/
        enemigoActual = 0;
        timeInicio = Time.time;
       
    }

    // Update is called once per frame
    void Update()
    {        
        float tiempoActual = Time.time - timeInicio;

        if (enemigoActual < apariciones.Length)
        {
            // Si el tiempo que ha pasado es mayor que cuando se supone que debe aparecer el enemigo actual, entonces...
            if(tiempoActual > apariciones[enemigoActual].tiempoInicio)
            {
                // Instanciamos el enemigo
                 int  indiceEnem = apariciones[enemigoActual].posEnemigo;
                GameObject prefabEnemigo = prefabsEnemigos[indiceEnem];

                    GameObject enemigo = GameObject.Instantiate(prefabsEnemigos[indiceEnem]);
                    float posX = apariciones[enemigoActual].posInicioX;
                    enemigo.transform.position = new Vector3(posX, esqSuperior.transform.position.y, 0);

                    enemigo.GetComponent<Enemigo>().controlJuego = this;
                    enemigo.GetComponent<Enemigo>().numEnemigo = enemigoActual;
                    enemigoActual++;
               
            }
        }

    }
    public void SumarPuntos(int puntos, int numEnem)
    {
        this.puntos += puntos;
        textoPuntos.text = "" + this.puntos;
        ComprobarFinDeJuego(numEnem);
    }
    public void RestarVida(int numEnem)
    {
        this.vidas--;
        textoVidas.text = "" + this.vidas;
        ComprobarFinDeJuego(numEnem);


    }
    private void ComprobarFinDeJuego(int numEnem)
    {
        if (this.vidas <= 0)
        {
            panelFinDeJuego.SetActive(true);
            panelPerdedor.SetActive(true);
            this.enabled = false;
        }
        else if (numEnem == this.apariciones.Length - 1)
        {
            panelFinDeJuego.SetActive(true);
            panelGanador.SetActive(true);
            this.enabled = false;
        }
    }
   


}
