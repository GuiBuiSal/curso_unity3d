﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoJugador : MonoBehaviour
{
    public float velocidad = 3f;
    public Transform esquinaIzquierda;
    public Transform esquinaDerecha;


    /** Si dirección vale
     *  +1  mueve derecha
     *  -1 mueve izquierda
     * 0 no se mueve */
    public void Mover (int direccion)
    {
        
        //DeltaTime = Tiempo (n) - Tiempo (n-1)

        Vector3 posicion = GetComponent<Transform>().position; // saca un numero
            posicion.x = posicion.x + direccion * velocidad * Time.deltaTime; // Alteramos un número
            // transform.position = posicion;  Le decimos al componente que use ese número, asigne a una propiedad           


        if (posicion.x < esquinaIzquierda.position.x)
        {
            posicion.x = esquinaIzquierda.position.x;
            
        }

        else if (posicion.x > esquinaDerecha.position.x)
        {
            posicion.x = esquinaDerecha.position.x;
            
        }
        transform.position = posicion; // Le decimos al componente que use ese número, asigne a una propiedad    
    }

    // Debug.Log("Time = " + Time.time * 1000 + " ms" + ", dT = " + Time.deltaTime * 1000 + " ms" + ", X = " + posicion.x);
    // EmularRetrasoCPU_GPU()
    /* private void EmularRetrasoCPU_GPU()
     {
         int vueltas = Random.Range(1, 50000000);
         for (int v= 0; v < vueltas; v++)
         {
             v = v * 1 / 1;
         }
     }*/
}
