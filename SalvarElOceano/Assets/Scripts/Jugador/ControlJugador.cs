﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    public AnimacionJugador animJug;

    private void Start()
    {
        animJug = GetComponent<AnimacionJugador>();        
    }


    // Update is called once per frame
    void Update()
    {
        // Si se pulsa la tecla flecha derecha:

        if (Input.GetKey(KeyCode.RightArrow)) 
        {
            // Invocamos al movimiento
            GetComponent<MovimientoJugador>().Mover(1);
            
            animJug.HaciaLaDerecha();

        }
        else if (Input.GetKey(KeyCode.LeftArrow)) 
        {
            GetComponent<MovimientoJugador>().Mover(-1);            
           
            animJug.HaciaLaIzquierda();
        }

        else  // Cuando no se pulsa tecla, paramos y ponemos idle
        {
            animJug.EnReposo();
        }
    }
}
