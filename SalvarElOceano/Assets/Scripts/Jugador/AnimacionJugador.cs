﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimacionJugador : MonoBehaviour
{
    // Cambiar la escala eje X a 1 o -1.
    public Transform transfSpritesCaballito;
    // Cambiar el parámetro dirección
    public Animator animCtrlCuerpo;
    // cambiar la velocidad
    public Animator animCtrlAlas;
    // cambiar el clip de animacion Legacy (antigua, heredada)
    public Animation animCabeza;

    public void HaciaLaDerecha()
    {
        transfSpritesCaballito.localScale = new Vector3(+1, 1, 1);
        animCtrlCuerpo.SetInteger("direccion", +1);
        animCtrlAlas.speed = 3f;
        animCabeza.CrossFade("Nadar_Cabeceo");
        
    }
    public void HaciaLaIzquierda()
    {
        transfSpritesCaballito.localScale = new Vector3(-1, 1, 1);
        animCtrlCuerpo.SetInteger("direccion", -1);
        animCtrlAlas.speed = 3f;
        animCabeza.CrossFade("Nadar_Cabeceo");
        
    }
    public void EnReposo()
    {
        animCtrlCuerpo.SetInteger("direccion", 0);
        animCabeza.CrossFade("Idle_Cabeceo");
        animCtrlAlas.speed = 1f;
    }
}
  