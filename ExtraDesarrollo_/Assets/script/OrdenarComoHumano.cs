﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrdenarComoHumano : MonoBehaviour
{


    public static void Main()
    {
        List<int> numeros = new List<int>();
        numeros.Add(4);
        numeros.Add(5);
        numeros.Add(2);
        numeros.Add(3);
        numeros.Add(1);

        BuscarMenor(numeros);
        //MostrarLista(numeros);

    }

    public static void MostrarLista(List<int> lista)
    {
        Console.WriteLine("Lista: ");

        for (int i = 0; i < lista.Count; i++)
        {
            Console.WriteLine(lista[i]);
        }
    }

    public static void BuscarMenor(List<int> lista)
    {
        int menor = lista[0];
        for (int i = 0; i < lista.Count; i++)
        {
            if (lista[i] > menor)
            {
                menor = lista[i];
            }
        }
        Console.WriteLine("El número menor de la lista es: " + menor);

    }

}
