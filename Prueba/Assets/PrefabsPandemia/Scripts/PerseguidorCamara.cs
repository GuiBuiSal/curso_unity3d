﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerseguidorCamara : MonoBehaviour
{
    public GameObject jugador;
    public Camera cam;
    public Transform limDer;

    private Vector3 posIni;
    private void Start()
    {
        posIni = jugador.transform.position - transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float x = Mathf.Lerp(this.transform.position.x, jugador.transform.position.x - posIni.x, .1f);
        if (x > limDer.position.x - 1f) x = limDer.position.x - 1f; 
        else if (x < 0f) x = 0f;

        this.transform.position = new Vector3(x, this.transform.position.y, this.transform.position.z);

        cam.orthographicSize -= Input.mouseScrollDelta.y / 5f;
        if (cam.orthographicSize > 3)
            cam.orthographicSize = 3;
        if (cam.orthographicSize < 1.8f)
            cam.orthographicSize = 1.8f;
    }
}
