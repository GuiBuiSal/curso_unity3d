﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoSprite : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.ToLower().Contains("suelo"))
        {
            this.GetComponent<Animator>().SetBool("impacto_suelo", true);
            this.transform.parent.GetComponent<DisparoGota>().enabled = false;
        } else
        {   // Suponemos que es enemigo
            GameObject murcielago = collision.gameObject;
            murcielago.GetComponent<Murcielago>();
            
        }
    }
    public void Destruir()
    {
        Destroy(this.transform.parent.gameObject);
    }
}
