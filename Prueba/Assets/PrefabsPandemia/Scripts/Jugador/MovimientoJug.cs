﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EstadoMov
{
    Suelo_Parado,
    Suelo_Moviendo,
    Saltando,
    Aire
}
public class MovimientoJug : MonoBehaviour
{
    public Vector2 mov;
    public Vector2 velocidad;
    public Vector2 velSalto = new Vector2(2, 4);
    public Vector2 velMov = new Vector2(2, 4);
    public float gravedad = -9f;
    public EstadoMov estado = EstadoMov.Suelo_Parado;
    public Transform trf;
    public SpriteRenderer spr;
    public Animator anim;
    public Cuerda cuerdaDer;
    public Cuerda cuerdaCen;
    public Cuerda cuerdaIzq;
    public Transform limiteIzq;
    public Transform limiteDer;
    public GameObject prefabDisparo;
    public Transform posDispDerIni;

    private GameObject gotaDisparo;

    // Start is called before the first frame update
    void Start()
    {
        trf = GetComponent<Transform>();
        cuerdaDer.gameObject.SetActive(false);
        cuerdaCen.gameObject.SetActive(false);
        cuerdaIzq.gameObject.SetActive(false);
        estado = EstadoMov.Suelo_Parado;
    }

    // Update is called once per frame
    void Update()
    {
        switch (estado)
        {
            case EstadoMov.Suelo_Parado:
                mov = Vector2.zero;
                //TODO: Comprobar si flipX tiene que estar así o qué
                spr.flipX = false;               
                velocidad = Vector2.zero;
                anim.SetBool("andar", false);
                anim.SetFloat("vel_y", velocidad.y);
                break;
            case EstadoMov.Suelo_Moviendo:
                anim.SetFloat("vel_y", 0f);
                break;
            case EstadoMov.Saltando:
                mov = velocidad * Time.deltaTime;
                break;
            case EstadoMov.Aire:
                mov = velocidad * Time.deltaTime;
                velocidad += new Vector2(0f, gravedad * Time.deltaTime);
                if (velocidad.y < 0)
                {
                    anim.speed = 1;
                } 
                anim.SetFloat("vel_y", velocidad.y);
                break;
        }

        trf.Translate(mov);

        if (trf.position.x < limiteIzq.position.x)
            trf.position = new Vector3(limiteIzq.position.x, trf.position.y, trf.position.z);
        if (trf.position.x > limiteDer.position.x)
            trf.position = new Vector3(limiteDer.position.x, trf.position.y, trf.position.z);
    }
    public void Parado()
    {
        estado = EstadoMov.Suelo_Parado;
    }
    public void Izquierda()
    {
        if (estado == EstadoMov.Suelo_Parado
            || estado == EstadoMov.Suelo_Moviendo)
        {
            estado = EstadoMov.Suelo_Moviendo;
            velocidad = new Vector2(-velMov.x, 0);
            mov = velocidad * Time.deltaTime;
            spr.flipX = true;
            anim.SetBool("andar", true);
        }
    }
    public void Derecha()
    {
        if (estado == EstadoMov.Suelo_Parado
            || estado == EstadoMov.Suelo_Moviendo)
        {
            estado = EstadoMov.Suelo_Moviendo;
            velocidad = new Vector2(velMov.x, 0);
            mov = velocidad * Time.deltaTime;
            spr.flipX = false;
            anim.SetBool("andar", true);
        }
    }
    public void Salto()
    {
        if (estado == EstadoMov.Suelo_Parado
            || estado == EstadoMov.Suelo_Moviendo)
        {
            estado = EstadoMov.Saltando;
            anim.SetFloat("vel_y", .01f);
            velocidad.y = velSalto.y / 20f;
        }
    }
    public void Disparo()
    {
        // Disparo sólo cuando no haya un disparo en curso
        if ( gotaDisparo == null)
        {
            gotaDisparo = GameObject.Instantiate(prefabDisparo);
            gotaDisparo.transform.parent = this.transform.parent;
            gotaDisparo.transform.position = posDispDerIni.position;
        }
    }
    public void SacarCuerda()
    {        
        if (velocidad.y > 0     // Solo cuando está subiendo
            && estado == EstadoMov.Aire
                                // Y todas las cuerdas estén inactivas
            && !cuerdaDer.gameObject.activeInHierarchy
            && !cuerdaCen.gameObject.activeInHierarchy
            && !cuerdaIzq.gameObject.activeInHierarchy)
        {
            if (velocidad.x > 0)
                cuerdaDer.gameObject.SetActive(true);
            else if (velocidad.x < 0)
                cuerdaIzq.gameObject.SetActive(true);
            else
                cuerdaCen.gameObject.SetActive(true);
        }
    }
}
