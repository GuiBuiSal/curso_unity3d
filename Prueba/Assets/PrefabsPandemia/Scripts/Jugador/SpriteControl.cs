﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteControl : MonoBehaviour
{
    public MovimientoJug movJug;
    public UnityEngine.UI.Text cargaViral;

    public void ComenzarSalto()
    {
        Debug.Log("EMP SALTO: " + Time.time);
        movJug.estado = EstadoMov.Aire;
        /*velocidad = new Vector2(Input.GetKey(KeyCode.A) ? -velSalto.x
                              : Input.GetKey(KeyCode.D) ? velSalto.x : 0,
                              velSalto.y);*/
        // Modificamos el vector de velocidad, hacia arriba, arriba-izq ó arr-der
        if (Input.GetKey(KeyCode.A))
            movJug.velocidad = new Vector2(-movJug.velSalto.x, movJug.velSalto.y);
        else if (Input.GetKey(KeyCode.D))
            movJug.velocidad = new Vector2(movJug.velSalto.x, movJug.velSalto.y);
        else
            movJug.velocidad = new Vector2(0f, movJug.velSalto.y);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("OnTriggerEnter2D: " + collision.name);
        if (collision.gameObject.name.ToLower().Contains("suelo"))
        {
            if (movJug.estado == EstadoMov.Aire)
            {
                movJug.estado = EstadoMov.Suelo_Parado;
                movJug.velocidad = Vector2.zero;
                movJug.anim.speed = 1;
                movJug.transform.localPosition = new Vector2( movJug.transform.localPosition.x, 0);

                movJug.anim.SetFloat("vel_y", 0f);
                movJug.anim.SetBool("andar", false);
                Debug.Log("TERM SALTO: " + Time.time);
            }
        } else
        {
            cargaViral.text = "" + (int.Parse(cargaViral.text) + 100);
        }
    }
}
