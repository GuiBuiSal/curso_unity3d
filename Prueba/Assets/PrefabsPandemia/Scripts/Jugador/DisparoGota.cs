﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoGota : MonoBehaviour
{
    public Vector2 velocidad;
    public Vector2 velIni = Vector2.right;
    public MovimientoJug movJug;
    private float gravedad;

    // Start is called before the first frame update
    void Start()
    {
        velocidad = velIni;
        gravedad = movJug.gravedad;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(velocidad * Time.deltaTime);
        velocidad.y += gravedad * Time.deltaTime;
    }
}
