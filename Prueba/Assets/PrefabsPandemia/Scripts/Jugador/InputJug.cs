﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputJug : MonoBehaviour
{
    public KeyCode teclaIzq = KeyCode.A;
    public KeyCode teclaDerecha = KeyCode.D;
    public KeyCode teclaSalto = KeyCode.W;
    public KeyCode teclaDisparo = KeyCode.Space;
    public KeyCode teclaCuerda = KeyCode.UpArrow;
    MovimientoJug movJug;
    private void Start()
    {
        movJug = this.GetComponent<MovimientoJug>();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(teclaDisparo) || Input.GetMouseButton(0))
        {
            movJug.Disparo();
        }
        if (Input.GetKey(teclaDerecha))
        {
            movJug.Derecha();
        }
        else if (Input.GetKey(teclaIzq))
        {
            movJug.Izquierda();
        } else
        {
            if (movJug.estado == EstadoMov.Suelo_Moviendo)
            {
                movJug.Parado();
            }
        }
        if (Input.GetKey(teclaSalto))
        {
            movJug.Salto();
        }

        if (Input.GetKey(teclaCuerda))
        {
            movJug.SacarCuerda();
        }            
    }
}
