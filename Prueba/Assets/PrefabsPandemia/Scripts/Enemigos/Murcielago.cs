﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Murcielago : MonoBehaviour
{
    public int cargaViral = 3;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RecibirDisparo()
    {
        cargaViral = cargaViral - 1;

        if (cargaViral == 0)
        {
            Sano();
        }
    }

    public void Sano()
    {
        this.GetComponent<Animator>().SetBool("CargaViral",true);
        this.transform.GetComponent<Animator>().enabled = false;

        // this.transform.gameObject.SetActive("CargaViral",true);
    }

}
