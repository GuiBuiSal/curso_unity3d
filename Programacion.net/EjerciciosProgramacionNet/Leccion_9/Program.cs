﻿using System;

namespace Leccion_9
{
    class Program
    {
        static void Main()
        {
            //Ejercicio1();
            //Ejercicio2();
            //Ejercicio3();
            // Ejercicio4();
            //Ejercicio5();
            //Ejercicio6();
            Ejercicio7();
        }
        public static void Ejercicio1()
        {
            // Escribir un programa que solicite ingresar 10 notas de alumnos y nos informe cuántos tienen notas mayores o iguales a 7 y cuántos menores. 
            
            string nota; 
            
            int contadorMasQue7 = 0;
            int contadorMenosQue7 = 0;
            int x;
            x = 1;
            while (x <= 10)
            {
                Console.WriteLine("Introduzca la nota del alumno: ");
                nota = Console.ReadLine();
                int numNota = int.Parse(nota);

                if (numNota >= 7)
                {
                    contadorMasQue7 = contadorMasQue7 + 1;
                }
                else { contadorMenosQue7 = contadorMenosQue7 + 1; }

                x = x + 1;
            }
            Console.WriteLine("Un total de " + contadorMasQue7 + " alumnos tienen una nota mayor que 7.");

            Console.WriteLine("Un total de " + contadorMenosQue7 + " alumnos tienen una nota menor que 7.");
        }
        public static void Ejercicio2()
        {
            // Se ingresan un conjunto de n alturas de personas por teclado. Mostrar la altura promedio de las personas. 

            string altura;
            string numeroAlturas;
            float sumatorio = 0f;

            Console.WriteLine("Inserte el número de alturas que desea introducir");
            numeroAlturas = Console.ReadLine();
            float numAlturas = float.Parse(numeroAlturas);
            
            int x = 1;
            while (x<= numAlturas)
            {
                Console.WriteLine("Inserte altura: ");
                altura = Console.ReadLine();
                float alturaCosa = float.Parse(altura);
               

                sumatorio = sumatorio + alturaCosa;

                x = x + 1;
            }
            float media = sumatorio / numAlturas;

            Console.WriteLine(" La altura media de las personas es: " + media);
        }
        public static void Ejercicio3()
        {
            //En una empresa trabajan n empleados cuyos sueldos oscilan entre $100 y $500, realizar un programa que lea los sueldos que cobra cada empleado e informe cuántos empleados cobran entre $100 y $300 y cuántos cobran más de $300. Además el programa deberá informar el importe que gasta la empresa en sueldos al personal. 

            string sueldo;
            string empleados;
            float contadorDineroMenosDe300 = 0f;
            float contadorDineroMasDe300 = 0f;
            float contadorEmpleadosMenosDe300 = 0f;
            float contadorEmpleadosMasDe300 = 0f;

            Console.WriteLine("Introduce el número de empleados que trabajan en esta empresa: ");
            empleados = Console.ReadLine();
            float numEmpleados = float.Parse(empleados);

            float x = 1f;
            while(x <= numEmpleados)
            {
                Console.WriteLine("Introduce el sueldo del empleado:");
                sueldo = Console.ReadLine();
                float numSueldo = float.Parse(sueldo);

                if(numSueldo >= 100 && numSueldo <= 300)
                {
                    contadorDineroMenosDe300 = contadorDineroMenosDe300 + numSueldo;
                    contadorEmpleadosMenosDe300 = contadorEmpleadosMenosDe300 + 1;
                }

                else if(numSueldo <= 500 && numSueldo > 300)
                {
                    contadorDineroMasDe300 = contadorDineroMasDe300 + numSueldo;
                    contadorEmpleadosMasDe300 = contadorEmpleadosMasDe300 + 1;
                }

                x = x + 1;
            }

            float salarioTotal = contadorDineroMenosDe300 + contadorDineroMasDe300;

            Console.WriteLine("El número de empleados que cobran entre 100 y 300 euros son: " + contadorEmpleadosMenosDe300);
            Console.WriteLine("El número de empleados que cobran más 300 euros son: " + contadorEmpleadosMasDe300);
            Console.WriteLine("El importe total ques se gasta la empresa en salarios son: " + salarioTotal + " euros.");

        }
        public static void Ejercicio4()
        {
            // Realizar un programa que imprima 25 términos de la serie 11 - 22 - 33 - 44, etc. (No se ingresan valores por teclado) 
            int contador = 0;

            int x = 0;
            while (x <= 25)
            {
                contador = contador + 11;
                Console.WriteLine(contador);
                x = x + 1;
            }
        }
        public static void Ejercicio5()
        {
            // Mostrar los múltiplos de 8 hasta el valor 500. Debe aparecer en pantalla 8 - 16 - 24, etc. 
            
            int x = 0;
            while (x<500)
            {
                x = x + 8;
                if(x <= 500) { Console.WriteLine(x); }                
            }
        }
        public static void Ejercicio6()
        {
            //Realizar un programa que permita cargar dos listas de 15 valores cada una. Informar con un mensaje cual de las dos listas tiene un valor acumulado mayor (mensajes "Lista 1 mayor", "Lista 2 mayor", "Listas iguales")
            //Tener en cuenta que puede haber dos o más estructuras repetitivas en un algoritmo. 
                        
            string valorUno;
            int valor1Total = 0;
            
            string valorDos;
            int valor2Total = 0;

            int x = 0;
            while (x <= 4) //Hasta x-1 ya que el primer valor empieza en 0
            {
                Console.WriteLine("Introduce el valor " + x + " para la lista 1");
                valorUno = Console.ReadLine();
                int valor1 = int.Parse(valorUno);
                valor1Total = valor1Total + valor1;

                Console.WriteLine("Introduce el valor " + x + " para la lista 2");
                valorDos = Console.ReadLine();
                int valor2 = int.Parse(valorDos);
                valor2Total = valor2Total + valor2;

                x = x + 1;
            }

            if (valor1Total > valor2Total) { Console.WriteLine("Lista 1 es mayor"); }
            if (valor1Total < valor2Total) { Console.WriteLine("Lista 2 es mayor"); }
            if (valor1Total == valor2Total) { Console.WriteLine("Listas iguales"); }
        }
        public static void Ejercicio7()
        {
            /*Desarrollar un programa que permita cargar n números enteros y luego nos informe cuántos valores fueron pares y cuántos impares.
                Emplear el operador “%” en la condición de la estructura condicional:

	                if (valor%2==0)         //Si el if da verdadero luego es par.*/

            string cargaDeNumeros;
            string numero;
            int contadorPar = 0;
            int contadorImpar = 0;

            Console.WriteLine("Introduce la cantidad de números enteros que quieras utilizar:");
            cargaDeNumeros = Console.ReadLine();
            int numCarga = int.Parse(cargaDeNumeros);

            int x = 1;
            while (x <= numCarga)
            {
                Console.WriteLine("Introduce un número entero");
                numero = Console.ReadLine();
                int num = int.Parse(numero);

                if (num % 2 == 0) 
                {
                    contadorPar = contadorPar + 1;
                    
                }
                else if(num % 2 != 0)
                {
                    contadorImpar = contadorImpar + 1;
                    
                }
                x = x + 1;
            }
            Console.WriteLine("Hay " + contadorPar + " números pares");
            Console.WriteLine("Hay " + contadorImpar + " números impares");
        }
    } 
}
