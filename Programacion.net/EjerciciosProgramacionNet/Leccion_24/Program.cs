﻿using System;

namespace Leccion_24
{
    class Program
    {
        static void Main()
        {
            // Confeccionar una clase para administrar una matriz irregular de 5 filas y 1 columna la primer fila, 2 columnas la segunda fila y así sucesivamente hasta 5 columnas la última fila (crearla sin la intervención del operador)
            // Realizar la carga por teclado e imprimir posteriormente. 
            OpeMatrices lolailo = new OpeMatrices();
            lolailo.AdmonMatriz();
            lolailo.Imprimir();

        }
    }
}
