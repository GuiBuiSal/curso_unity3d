﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leccion_24
{
    class OpeMatrices
    {

        // Confeccionar una clase para administrar una matriz irregular de 5 filas y 1 columna la primer fila, 2 columnas la segunda fila y así sucesivamente hasta 5 columnas la última fila (crearla sin la intervención del operador)
        // Realizar la carga por teclado e imprimir posteriormente. 

        private int[][] matrioska;

        public void AdmonMatriz()
        {
            // Hay que hacerlo con un bucle
            matrioska = new int[5][];
            matrioska[0] = new int[1];
            matrioska[1] = new int[2];
            matrioska[2] = new int[3];
            matrioska[3] = new int[4];
            matrioska[4] = new int[5];


            for (int f = 0; f < matrioska.Length; f++)
            {
                for(int c = 0; c < matrioska[f].Length; c++)
                {
                    Console.WriteLine("Ingrese el valor de la componente " + f + "," + c);
                    matrioska[f][c] = int.Parse(Console.ReadLine());
                }
            }
            Console.WriteLine("");
        } // Fin de AdmonMatriz()

        public void Imprimir()
        {
            for (int f = 0; f < matrioska.Length; f++)
            {
                for (int c = 0; c < matrioska[f].Length; c++)
                {
                    Console.Write(matrioska[f][c] + " ");
                    
                }
            }Console.WriteLine();
        }


    }
}
