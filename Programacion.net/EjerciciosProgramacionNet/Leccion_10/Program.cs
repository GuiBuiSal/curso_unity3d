﻿using System;

namespace Leccion_10
{
    class Program
    {
        static void Main()
        {
            //Ejercicio1();
            // Ejercicio2();
            //Ejercicio3();
            //Ejercicio4();
            //Ejercicio5();
            //Ejercicio6();
            //Ejercicio7();
            Ejercicio8();

        }
        public static void Ejercicio1()
        {
            /* Confeccionar un programa que lea n pares de datos, cada par de datos corresponde a la medida de la base y la altura de un triángulo. El programa deberá informar:
                a) De cada triángulo la medida de su base, su altura y su superficie área.
                b) La cantidad de triángulos cuya superficie es mayor a 12. */
            string paresDatos;
            string baseTriangulo;
            string alturaTriangulo;
            int contadorMas12 = 0;
            int contadorMenos12 = 0;

            Console.WriteLine("Introduzca la cantidad de triángulos para hacer las operaciones");
            paresDatos = Console.ReadLine();
            int numParesDatos = int.Parse(paresDatos);

            for (int i = 0; i < numParesDatos; i++)
            {
                Console.WriteLine("Introduzca altura del triángulo número " + (i + 1));
                alturaTriangulo = Console.ReadLine();
                int numAlturaTriangulo = int.Parse(alturaTriangulo);

                Console.WriteLine("Introduzca base del triángulo número " + (i + 1));
               baseTriangulo = Console.ReadLine();
                int numBaseTriangulo = int.Parse(baseTriangulo);

                int area = (numBaseTriangulo * numAlturaTriangulo) / 2;

                if (area > 12)
                {
                    contadorMas12 = contadorMas12 + 1;
                }
                else { contadorMenos12 = contadorMenos12 + 1; }
                  
                Console.WriteLine("Triángulo " + (i + 1) + " : la base del triángulo es " + numBaseTriangulo + " y la altura del triángulo es " + numAlturaTriangulo + " y el área del triángulo es " + area);
                
            }
            Console.WriteLine("El número de triángulos cuya superficie es mayor a 12 son " + contadorMas12 + " y el número de triángulos cuya superficie es menor a 12 son " + contadorMenos12);
        }
        public static void Ejercicio2() 
        {
            //Desarrollar un programa que solicite la carga de 10 números e imprima la suma de los últimos 5 valores ingresados. 

            string numero;
            int sumarnum = 0;
            for(int i = 0; i <= 9; i++)
            {
                Console.WriteLine("Inserte un número (posición" + (i + 1) + ").");
                numero = Console.ReadLine();
                int num = int.Parse(numero);

                if (i >= 5)
                {
                    sumarnum = sumarnum + num;
                }
            }
            Console.WriteLine("La suma de los últimos 5 valores ingresados es: " + sumarnum);
        }
        public static void Ejercicio3()
        {
            // Desarrollar un programa que muestre la tabla de multiplicar del 5 (del 5 al 50) 
            Console.WriteLine("La tabla de multiplicar del 5 es:");
            for (int i = 1; i<=10; i++)
            {
                Console.WriteLine(5 * i);
            }
        }
        public static void Ejercicio4()
        {
            /* Confeccionar un programa que permita ingresar un valor del 1 al 10 y nos muestre la tabla de multiplicar del mismo (los primeros 12 términos)
                Ejemplo: Si ingreso 3 deberá aparecer en pantalla los valores 3, 6, 9, hasta el 36. */
            string numero;

            Console.WriteLine("Introduzca in valor del 1 al 10");
            numero = Console.ReadLine();
            int num = int.Parse(numero);           

            if (num > 1 && num < 10)
            {
                Console.WriteLine("La tabla del " + num + " es: ");

                for (int i = 1; i <= 12; i++)
                {
                    Console.WriteLine(num * i);
                }
            }            
        }
        public static void Ejercicio5()
        {
            /*Realizar un programa que lea los lados de n triángulos, e informar:
            a) De cada uno de ellos, qué tipo de triángulo es: equilátero (tres lados iguales), isósceles (dos lados iguales), o escaleno (ningún lado igual)
            b) Cantidad de triángulos de cada tipo.
            c) Tipo de triángulo que posee menor cantidad. */

            string triangulo;
            int contadorEquilatero = 0;
            int contadorIsosceles = 0;
            int contadorEscaleno = 0;

            Console.WriteLine("Introduzca el número de triángulos");
            triangulo = Console.ReadLine();
            int numTriangulo = int.Parse(triangulo);

            for(int i = 1; i <= numTriangulo; i++)
            {
                Console.WriteLine("Los lados del triángulo " + i  + " son:");

                Console.WriteLine("Introduzca el lado 1");
               string ladoUno = Console.ReadLine();
                int numLado1 = int.Parse(ladoUno);

                Console.WriteLine("Introduzca el lado 2");
                string ladoDos = Console.ReadLine();
                int numLado2 = int.Parse(ladoDos);

                Console.WriteLine("Introduzca el lado 3");
                string ladoTres = Console.ReadLine();
                int numLado3 = int.Parse(ladoTres);

                if(numLado1 == numLado2 && numLado1 == numLado3)
                {
                    contadorEquilatero = contadorEquilatero + 1;
                }
                else if (numLado1 == numLado2 || numLado1 == numLado3 || numLado2 == numLado3)
                {
                    contadorIsosceles = contadorIsosceles + 1;
                }
                else if (numLado1 != numLado2 && numLado1 != numLado3 && numLado2 != numLado3)
                {
                    contadorEscaleno = contadorEscaleno + 1;
                }
            }
            if(contadorEquilatero < contadorIsosceles && contadorEquilatero < contadorEscaleno)
            {
                Console.WriteLine("El tipo de triángulo que posee menor cantidad es el equilátero.");
            }
            else if (contadorIsosceles < contadorEquilatero && contadorIsosceles < contadorEscaleno)
            {
                Console.WriteLine("El tipo de triángulo que posee menor cantidad es el isósceles.");
            }
            else 
            {
                Console.WriteLine("El tipo de triángulo que posee menor cantidad es el escaleno.");
            }

        }
        public static void Ejercicio6()
        {
            // Escribir un programa que pida ingresar coordenadas (x,y) que representan puntos en el plano.
            // Informar cuántos puntos se han ingresado en el primer, segundo, tercer y cuarto cuadrante. Al comenzar el programa se pide que se ingrese la cantidad de puntos a procesar.

            string coordenadas;
            int contador1cuad = 0;
            int contador2cuad = 0;
            int contador3cuad = 0;
            int contador4cuad = 0;

            Console.WriteLine("Ingrese el número de coordenadas que quiera poner en la gráfica");
            coordenadas = Console.ReadLine();
            int numCoordenadas = int.Parse(coordenadas);

            for (int i = 1; i <= numCoordenadas; i++) 
            {
                Console.WriteLine("Ingrese el valor de X en el punto " + i + ":");
                string x = Console.ReadLine();
                int numX = int.Parse(x);

                Console.WriteLine("Ingrese el valor de Y en el punto " + i + ":");
                string y = Console.ReadLine();
                int numY = int.Parse(y);

                if(numX > 0 && numY > 0) { contador1cuad = contador1cuad + 1; }
                else if (numX < 0 && numY > 0) { contador2cuad = contador2cuad + 1; }
                else if (numX < 0 && numY < 0) { contador3cuad = contador3cuad + 1; }
                else if (numX > 0 && numY < 0) { contador4cuad = contador4cuad + 1; }
            }

            Console.WriteLine("Ha habido " + contador1cuad + " puntos en el primer cuadrante, " + contador2cuad + " puntos en el segundo cuadrante, " + contador3cuad + " puntosen el tercer cuadrante y " + contador4cuad + " puntos en el cuarto cuadrante.");
        }
        public static void Ejercicio7()
        {
            /*Se realiza la carga de 10 valores enteros por teclado. Se desea conocer:
            a) La cantidad de valores ingresados negativos.
            b) La cantidad de valores ingresados positivos.
            c) La cantidad de múltiplos de 15.
            d) El valor acumulado de los números ingresados que son pares. */

            int contadorPositivos = 0;
            int contadorNegativos = 0;
            int contador15 = 0;
            int sumaDePares = 0;
            int sumaDeParesMenos = 0;
            int paresTotal = 0;

            for (int i = 1; i <= 10; i++)
            {
                Console.WriteLine("Introduce el valor " + i);
                string valor = Console.ReadLine();
                int numValor = int.Parse(valor);

                if(numValor < 0) 
                { 
                    contadorNegativos = contadorNegativos + 1; 
                    if (numValor % 15 == 0) { contador15 = contador15 + 1; }
                    if (numValor % 2 == 0) { sumaDePares = sumaDePares + numValor; }
                }
                else if (numValor > 0) 
                { 
                    contadorPositivos = contadorPositivos + 1;
                    if (numValor % 15 == 0) { contador15 = contador15 + 1; }
                    if (numValor % 2 == 0) { sumaDeParesMenos = sumaDeParesMenos + numValor; }
                }
                paresTotal = sumaDeParesMenos + sumaDePares;
            }

            Console.WriteLine("La cantidad de valores negativos ingresados son " + contadorNegativos);
            Console.WriteLine("La cantidad de valores positivos ingresados son " + contadorPositivos);
            Console.WriteLine("La cantidad de valores múltiplos de 15 ingresados son " + contador15);
            Console.WriteLine("El valor acumulado de los números pares ingresados son " + paresTotal);
        }
        public static void Ejercicio8()
        {
            /* Se cuenta con la siguiente información:
                Las edades de 3 estudiantes del turno mañana.
                Las edades de 4 estudiantes del turno tarde.
                Las edades de 5 estudiantes del turno noche.
                Las edades de cada estudiante deben ingresarse por teclado.
                    a) Obtener el promedio de las edades de cada turno (tres promedios)
                    b) Imprimir dichos promedios (promedio de cada turno)
                    c) Mostrar por pantalla un mensaje que indique cual de los tres turnos tiene un promedio de edades mayor. */

            int estudiantesMorning = 3;
            int contadorMorning = 0;
            int mediaMorning = 0;
            int estudiantesTarde = 4;
            int contadorTarde = 0;
            int mediaTarde = 0;
            int estudiantesNoche = 5;
            int contadorNoche = 0;
            int mediaNoche = 0;

            for (int i = 1; i <= estudiantesMorning; i++)
            {
                Console.WriteLine("Ingrese la edad del alumno " + i + " del turno de mañana");
                int edadMorning = int.Parse(Console.ReadLine());
                contadorMorning = contadorMorning + edadMorning;
            }
            mediaMorning = contadorMorning / estudiantesMorning;
            

            for (int i = 1; i <= estudiantesTarde; i++)
            {
                Console.WriteLine("Ingrese la edad del alumno " + i + " del turno de tarde");
                int edadTarde = int.Parse(Console.ReadLine());
                contadorTarde = contadorTarde + edadTarde;
            }
            mediaTarde = contadorTarde / estudiantesTarde;
           

            for (int i = 1; i <= estudiantesNoche; i++)
            {
                Console.WriteLine("Ingrese la edad del alumno " + i + " del turno de noche");
                int edadNoche = int.Parse(Console.ReadLine());
                contadorNoche = contadorNoche + edadNoche;
            }
            mediaNoche = contadorNoche / estudiantesNoche;

            Console.WriteLine("La media de edad de los estudiantes de la mañana es de " + mediaMorning);
            Console.WriteLine("La media de edad de los estudiantes de la tarde es de " + mediaTarde);
            Console.WriteLine("La media de edad de los estudiantes de la tarde es de " + mediaNoche);

            if (mediaMorning > mediaTarde && mediaMorning > mediaNoche) { Console.WriteLine("El turno de mañana tiene la media de edad mayor con un promedio de " + mediaMorning); }
            if (mediaTarde > mediaMorning && mediaTarde > mediaNoche) { Console.WriteLine("El turno de tarde tiene la media de edad mayor con un promedio de " + mediaTarde); }
            if (mediaNoche > mediaMorning && mediaNoche > mediaTarde) { Console.WriteLine("El turno de noche tiene la media de edad mayor con un promedio de " + mediaNoche); }

           
        }
    }
}
