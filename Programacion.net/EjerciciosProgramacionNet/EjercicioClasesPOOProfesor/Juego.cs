﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjercicioClasesPOOProfesor
{
    class Juego
    {
        public Unidad jugador = null;

        List<Unidad> todoEnemigos;

        public void Inicializar()
        {

            // La lista es un objeto y hay que darle un valor de lista vacía

            this.todoEnemigos = new List<Unidad>();

            //this.enemigo = new Unidad("Ctchulu", 50, 10);
            //this.otroEnem = new Unidad("Furia", 18,30);
            //this.enem2 = new Unidad("Bégimo", 70, 20,25);
                        
            this.todoEnemigos.Add(new Unidad("Ctchulu", 50, 10));
            this.todoEnemigos.Add(new Unidad("Furia", 18, 30));
            this.todoEnemigos.Add(new Unidad("Bégimo", 70, 20, 25));


            // Crear otro enemigo, cuya vida sea 100, con otro nombre y ataque. DEPURAR para COMPROBAR
            this.jugador = new Unidad();
            //Console.WriteLine("Dí tu nombre:");
            this.jugador.nombre = "Sir William"; //Console.ReadLine();
                                                 // Console.WriteLine("Indica tu ataque:");
                                                 //this.jugador.ataque = 30; //int.Parse(Console.ReadLine());
            this.jugador.SetAtaque(30);
            //this.jugador.vida = 300;
            this.jugador.SetVida(400);
            this.jugador.SetVidaMaxima(400);

            
                               
           

            
           /* for(int i = 0; i<3; i++)
            {
                int nuevaVida = new Random().Next(10, 15);
                int nuevoAtaque = new Random().Next(5, 10);
                int nuevaPocion = new Random().Next(0, 2);
                otroEnem = new Unidad("Enem " + i, nuevaVida, nuevoAtaque, nuevaPocion);
                this.todoEnemigos.Add(otroEnem);
            }*/


        }
        public void MostrarUnidades()
        {
            Console.WriteLine("");

            for(int i = 0; i < todoEnemigos.Count; i++)
            {
                Console.WriteLine(todoEnemigos[i].EnTexto());
            }
            /*//jugador.Mostrar("JUGADOR");
            Console.WriteLine(jugador.EnTexto());
            //enemigo.Mostrar("ENEMIGO");
            Console.WriteLine(enemigo.EnTexto());
            //otroEnem.Mostrar("ENEMIGO");
            Console.WriteLine(otroEnem.EnTexto());
            Console.WriteLine(enem2.EnTexto());
            Console.WriteLine("");*/
        }
        // Programar un método: ComenzarAtaques(), que haga que enemigo ataque a jugador, y jugador a enemigo.

        /*public void ComenzarAtaques()
        {
            enemigo.AtacaA(this.jugador);  // Enemigo ataca a jugador
            jugador.AtacaA(this.enemigo);  // Viceversa           
        }*/
        /*  public void Enemigo1AtacaA()
         {
           enemigo.AtacaA(jugador);

           Console.WriteLine();
         }
         public void Enemigo2AtacaA()
         {
            otroEnem.AtacaA(jugador);

            Console.WriteLine();
         }
         public void PersonajeAtacaA()
         {
             jugador.AtacaA(enemigo);

             jugador.AtacaA(otroEnem);

             Console.WriteLine();
         }*/

        public void ContinuarAtaques()
        {   
            for(int i = 0; i < todoEnemigos.Count; i++)
            {
                todoEnemigos[i].AtacaA(jugador);
                jugador.AtacaA(todoEnemigos[i]);
                
            }
          /* todoEnemigos[0].AtacaA(jugador);
            jugador.AtacaA(todoEnemigos[0]);
            todoEnemigos[1].AtacaA(jugador);
           jugador.AtacaA(todoEnemigos[1]);
            todoEnemigos[2].AtacaA(jugador);
           jugador.AtacaA(todoEnemigos[2]);
           MostrarUnidades();*/
        }

        public void CurarVida()
        {
            if (jugador.SiEstaVivo() == true)
            {
                if(jugador.GetVida() <= jugador.GetVidaMaxima() * 0.5) 
                {
                    jugador.DarPocion(jugador);
                    Console.WriteLine("La vida del jugador se ha aumentado hasta: " + jugador.GetVida());
                }                
            }
        }

        public void CurarVidaEnemigo()
        {
            //TODO Ejercicio 9: Que un enemigo aleatorio cure a otro enem aletorio
            // Buscar en Internet   "Random int C#"

            int idxEnemigoCurandero;
            int idxEnemigoCurado;

            do
            {
                // Números PSEUDO-ALEATORIOS
                idxEnemigoCurandero = new Random().Next(this.todoEnemigos.Count);
                idxEnemigoCurado = new Random().Next(this.todoEnemigos.Count);
                

            } while (idxEnemigoCurandero == idxEnemigoCurado);

            Unidad curandero = this.todoEnemigos[idxEnemigoCurandero];
            Unidad curado = this.todoEnemigos[idxEnemigoCurado];
            curandero.DarPocion(curado);

            /*{
                if (todoEnemigos[idxEnemigoCurandero].GetVida() <= todoEnemigos[idxEnemigoCurado].GetVidaMaxima() * 0.20)
                {

                    idxEnemigoCurandero.DarPocion(idxEnemigoCurado);
                    Console.WriteLine("------------" + todoEnemigos[idxEnemigoCurandero].nombre + " ha curado la vida de " + todoEnemigos[idxEnemigoCurado].nombre + " que ha aumentado hasta: " + todoEnemigos[idxEnemigoCurado].GetVida() + " puntos de vida.");

                }

            }*/

        }
        /*public int GanarPerder()
        {
            if(enemigo.SiEstaVivo() == true || otroEnem.SiEstaVivo() == true) 
            { 
                Console.WriteLine("Seguimos luchando, sigue vivo");
                luchar = 1;
                return luchar;
            }
            else 
            {
                Console.WriteLine("You win"); luchar = 2;
                return luchar;
            }
                      
        }*/
        public bool SiGameOver()
        {
            // if (UnidadEstaViva(this.jugador))
            if (this.jugador.SiEstaVivo() == true)
            {
                Console.WriteLine("Jugador sigue vivo");
                return false;
            }
            else
            {
                Console.WriteLine("GAME OVER!");
                return true;                
            }
        }
        public bool SiGameWIN()
        {
            //TODO Ejercicio 9: Pista: Usar una variable booleana para 
            // que cambie en el bucle que toca hacer

            bool siHayAlgunoVivo = false;

            for(int i =0; i < todoEnemigos.Count; i++) 
            {
                if (this.todoEnemigos[i].SiEstaVivo())
                {
                    siHayAlgunoVivo = true;
                    // Prodríamos romper el bucle una vez encontrado uno
                    break;
                }

            }
            if (siHayAlgunoVivo)
            {
                Console.WriteLine("Todavía enemigo vivo, seguimos luchando");
                return false;
            }
            else // if ( ! this.enemigo.EstaVivo() && ! this.otroEnemigo.EstaVivo())
            {
                Console.WriteLine("GAME WIN!");
                return true;
            }
            
        }
    }
}

