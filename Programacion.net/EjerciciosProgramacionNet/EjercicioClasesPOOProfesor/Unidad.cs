﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjercicioClasesPOOProfesor
{
    class Unidad
    {
        public String nombre;
        private int vida;
        private int ataque;


        private int pocion;
        private int vidaTotal;
        private int contadorMuertos;
        
        #region Constructores:
        // Constructores: SOn métodos especiales para construir, No devuelven nada concreto, sólo un objeto del tipo de la propia clase
        // Constructor por defecto:
        public Unidad()
        {
            this.nombre = "Unidad sin nombre";
            this.vida = 100;
            this.ataque = 10;
            this.pocion = 30;
            this.vidaTotal = this.vida;
            this.contadorMuertos = 0;
           
    }
        // Constructor con parámetros
        public Unidad(String nombre, int nuevaVida, int ataque,  int pocion)
        {
            this.nombre = nombre;
            this.vida = nuevaVida;
            this.ataque = ataque;
            this.vidaTotal = this.vida;
            this.pocion = pocion;
            this.contadorMuertos = 0;
            
        }
        // Crear un constructor donde la vida por defecto se ponga a 100
        public Unidad(String nombre, int ataque, int pocion)
        {
            this.nombre = nombre;
            this.vida = 100;
            this.ataque = ataque;
            this.vidaTotal = this.vida;
            this.contadorMuertos = 0;
           
            this.pocion = pocion;


        } 
        // End Unidad()
        #endregion

        #region Métodos de visualización
        public String EnTexto()
        {
            return "Nombre Unidad:" + this.nombre + ", Vida/Ataque: " + this.vida + "/" + this.ataque;
        }
        public void Mostrar(String tipoUnidad)
        {
            Console.WriteLine("Unidad " + tipoUnidad + " :" + this.nombre);
            Console.WriteLine("Vida/Ataque: " + this.vida + "/" + this.ataque);
        } //en visualización
        #endregion

        #region Otros métodos de juego
        // Crear un método RecibirAtaque(), que quite X vida (parámetro) a la vida del objeto (variable miembro)
       /* public void RecibirAtaque(int x_ataque)
        {
            this.vida = this.vida - x_ataque;
        }*/
        public void AtacaA(Unidad unidRecibe)
        {
            if (SiEstaVivo() == true && unidRecibe.SiEstaVivo() == true) // Otra forma this.vida>0;
            {
                unidRecibe.vida = unidRecibe.vida - this.ataque;
                if (unidRecibe.vida <= 0) { unidRecibe.vida = 0; }
                Console.WriteLine(this.nombre + " está vivo y ataca a " + unidRecibe.nombre + " ( " + unidRecibe.vida + " ) " );                                    
            }
             else 
             {
                 if (this.contadorMuertos < 1) //bool mensajeMostrado-- if(!mensajeMostrado) tal tal tal tal;
                 {
                     this.contadorMuertos = this.contadorMuertos + 1;   
                     Console.WriteLine("Alguien está muerto " + this.nombre + " no ataca"); 
                 }                                
             }
        }
        public bool SiEstaVivo()
        {
            if (this.vida > 0) 
            { 
                return true;               
            }
            else { return false; }
        }

        public void DarPocion(Unidad unidCurado)
        {
            if (SiEstaVivo() && unidCurado.SiEstaVivo())
            {
                unidCurado.vida += this.pocion;
                if (unidCurado.vida > unidCurado.vidaTotal)
                {
                    unidCurado.vida = unidCurado.vidaTotal;
                }
                Console.WriteLine(" >>>" + nombre + " ha curado a " + unidCurado.nombre
                    + "(" + unidCurado.vida + ")");
            }
        }
        #endregion

        #region Getters y Setters 
        public int GetVidaMaxima()
        {
            return this.vidaTotal;
        }
        public void SetVidaMaxima(int vm)
        {
            if (this.vida > vm)
            {
                this.vida = vm;
            }
            if (vm < 0)
                vm = 0;
            this.vidaTotal = vm;
        }

        public int GetVida()
        {
            return this.vida;
        }
        public void SetVida(int vd)
        {
            this.vida = vd;
        }
        
        // Ataque con formato Getter y Setter de C#: Formato propiedad
        // nombre de variable miembro, la primera con mayúscula
        /*public int Ataque  // Una propiedad en .Net es un par de métodos get y set con una estructura predefinida, pero que se usan como campos, como variables miembro.
        {
            get
            {
                return this.ataque;
            }
            set
            {
                if (value < 0) 
                    value = 0;
                this.ataque = value; // En el setter, value corresponde al valor asignado
            }

        } //end Ataque*/
       public int GetAtaque()
        {
            return this.ataque;
        }
        public void SetAtaque(int atak)
        {
            this.ataque = atak;
        }
        public String GetNombre()
        {
            return this.nombre;
        }
        public void SetNombre(string nom)
        {
            if (nom == "" || nom == null)
            {
                throw new Exception("Error, no puede estar vacío.");
            }
            this.nombre = nom;
        }
        public int GetSetPocion
        {
            get
            {
                return this.pocion;
            }
            set
            {
                if (value < 0)
                    value = 0;
                this.pocion = value;
            }
        }

        #endregion
    } // end class

}   // end Namespace
