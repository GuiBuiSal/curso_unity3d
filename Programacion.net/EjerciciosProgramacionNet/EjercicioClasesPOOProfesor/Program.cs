﻿using System;
// Mini juego: 
// Fase 1: Como jugadores tenemos una unidad, y luchamos contra 1 enemigo

namespace EjercicioClasesPOOProfesor
{
    // La clase Program la hacemos static para que NO se pueda instaciar ni usar como objeto.
    /*
    x1x A la hora de atacar comprobar que el que ataca está vivo 
    x2x Meter un segundo enemigo, el jugador ataca a los 2 enemigos y estos atacan al jugador
    x3x Nuevo campo en unidad pocion de tipo entero, que habrá que añadir en los constructores, dar un valor...
    4 un nuevo método Curar a (Unidad) para que una unidad cure a otra, en función del valor que tenda como poción
    5 Que el juego se repita hasta que ganes o pierdas

    /* Ejercicios:
         * 1)-- A la hora de atacar, comprobar que quien ataca está vivo (si está muerto no puede atacar)         
         *          En Unidad -> AtacaA()
         *           Extra: No atacar si a quién ataca está también. Es decir, si cualquiera está muerto (quién ataca o a quién se ataca) no se puede atacar. 
         *   Que la vida no puede ser negativa
         * 2)-- Meter un segundo enemigo: El jugador ataca a los 2 enemigos, y estos atacan al jugador     
         *          En el Juego -> Varios métodos
         * 3)-- Nuevo campo en Unidad pocion de tipo entero, habrá que añadir en 
         *    los constructores, dar un valor...
         *          En Unidad
         * 4)-- Crear una vidaMax, que guarde la vida inicial.
         *          En Unidad
         * 5) Un nuevo método CurarA(Un) para que una unidad cure a otra, en función del valor que tenga como poción. 
         *          En Unidad
         * 6) Hacer que cuando cure, no sobrepase la vidaMax. Sólo si está vivo
         *          En el método Unidad -> CurarA()
         * 7) Hacer que el enemigo 1 cure al 2, por ejemplo, dos veces
         *          En el Juego
         * 8) Que el juego se repita hasta que ganes o pierdas. 
         *          En Main()
         * 9) Hacer que hayan 3 enemigos con opción a que hayan 30:
         *    Tiene que ser un array (o lista) de Unidades 
         * 10) Poner todo en private, y crear y usar getter y setters
         * */

    static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Juego!");
            Juego miJuego = new Juego();
            miJuego.Inicializar();
            miJuego.MostrarUnidades();
            Console.WriteLine();

            do
            {
                miJuego.ContinuarAtaques();
                miJuego.CurarVida();
                miJuego.CurarVidaEnemigo();
                miJuego.SiGameOver();
                miJuego.SiGameWIN();

            } while (!miJuego.SiGameOver() && !miJuego.SiGameWIN());
            

            

        }
    }
}
