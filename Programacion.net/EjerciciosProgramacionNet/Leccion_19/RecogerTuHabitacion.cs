﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leccion_19
{
    class RecogerTuHabitacion
    {
        //Cargar un vector de n elementos de tipo entero.Ordenar posteriormente el vector.

        private int tamano = 0;
        private int[] numeros;
        public void CantidadDeVector()
        {
            
            Console.WriteLine("Inserte el tamaño del vector");
            tamano = int.Parse(Console.ReadLine());

        }
        public void LLenarArray()
        {
            numeros = new int[tamano];
            for (int i = 0; i < tamano; i++)
            {
                Console.WriteLine("Insertar el valor de la posición " + i + " del array");
                numeros[i] = int.Parse(Console.ReadLine());
            }
        }
        public void Ordenar()
        {
            Console.WriteLine();

            for (int i = 0; i < tamano; i++) 
            {
                for(int k = i + 1 ; k <= tamano-1; k++)
                {
                    if (numeros[i] > numeros[k])
                    {
                        int varAux = 0;
                        varAux  = numeros[k];
                        numeros[k] = numeros[i];
                        numeros[i] = varAux;
                    }
                }
            }
        }
        public void Mostrar()
        {
            Console.WriteLine();
            for (int i = 0; i < tamano; i++) 
            {
                Console.WriteLine(numeros[i]);
            }
        }


    }
}
