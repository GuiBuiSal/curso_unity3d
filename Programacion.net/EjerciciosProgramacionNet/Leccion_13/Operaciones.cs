﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Leccion_13
{
    //Implementar la clase operaciones. Se deben cargar dos valores enteros, calcular su suma, resta, multiplicación y división, cada una en un método, imprimir dichos resultados. 

    class Operaciones
    {
        private int num1, num2, suma, resta, multiplicar, dividir;
       
        
        public void CargaDatos()
        {
            Console.WriteLine("Introduce un número entero");
            num1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Introduce otro número entero");
            num2 = int.Parse(Console.ReadLine());

        }
        
        public void Sumar()
        {
            suma = num1 + num2;           
        }
        public void Restar()
        {
            resta = num1 - num2;
        }
        public void Multiplicar()
        {
            multiplicar = num1 * num2;
        }
        public void Dividir()
        {
            dividir = num1 / num2;
        }
        public void Resultados()
        {
            Console.WriteLine("El resultado de la suma es " + suma + ", el resultado de la resta es " + resta + ", el resultado de la multiplicación es " + multiplicar + " y el resultado de la división es " + dividir);
        }
    }
}
