﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leccion_13
{
    // Confeccionar una clase que represente un empleado. Definir como atributos su nombre y su sueldo. Confeccionar los métodos para la carga, otro para imprimir sus datos y por último uno que imprima un mensaje si debe pagar impuestos (si el sueldo supera a 3000) 

    class Empleado
    {
        private string nombre;
        private int sueldo;

        public void CargaDatos()
        {
            Console.WriteLine("Introduce el nombre del empleado:");
            nombre = Console.ReadLine();

            Console.WriteLine("Introduce el sueldo del empleado");
            sueldo = int.Parse(Console.ReadLine());
        }
        public void ImpresionDatos()
        {
            Console.WriteLine("El empleado " + nombre + " tiene un sueldo de " + sueldo);
        }

        public void PagarImpuestos()
        {
            if (sueldo > 3000) { Console.WriteLine("Debes pagar impuestos."); }
            else { Console.WriteLine("No debes pagar impuestos");}
        }



    }
}
