﻿using System;

namespace Leccion_13
{
    class Program
    {
        static void Main()
        {
            // Ejercicio1();
            Ejercicio2();
        }
        public static void Ejercicio1()
        {
            // Confeccionar una clase que represente un empleado. Definir como atributos su nombre y su sueldo. Confeccionar los métodos para la carga, otro para imprimir sus datos y por último uno que imprima un mensaje si debe pagar impuestos (si el sueldo supera a 3000) 

            Empleado trabajador = new Empleado();

            trabajador.CargaDatos();
            trabajador.ImpresionDatos();
            trabajador.PagarImpuestos();
        }
        public static void Ejercicio2()
        {
            //Implementar la clase operaciones. Se deben cargar dos valores enteros, calcular su suma, resta, multiplicación y división, cada una en un método, imprimir dichos resultados. 
            Operaciones cuentas = new Operaciones();
            cuentas.CargaDatos();
            cuentas.Sumar();
            cuentas.Restar();
            cuentas.Multiplicar();
            cuentas.Dividir();
            cuentas.Resultados();
        }
    }
}
