﻿using System;

namespace Leccion_21
{
    class Program
    {
        static void Main()
        {

            // Crear una matriz de 2 filas y 5 columnas. Realizar la carga de componentes por columna (es decir primero ingresar toda la primer columna, luego la segunda columna y así sucesivamente)
            // Imprimir luego la matriz.
            Operaciones matriciarias = new Operaciones();
            
            matriciarias.CrearMatriz();
            matriciarias.CargarMatriz();
            matriciarias.ImprimirMatriz();

        }
    }
}
