﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Leccion_21
{
    class Operaciones
    {
        private int[,] matriz;

        public void CrearMatriz() 
        {
            matriz = new int[2, 5];
        }
        public void CargarMatriz()
        {
            for (int i=0; i < 5; i++)
            {
                for (int j=0; j < 2; j++) 
                {
                    Console.WriteLine("Ingrese componente " + " de la fila " + j + " y la columna " + i + " :");
                    matriz[j, i] = int.Parse(Console.ReadLine());
                }
            }
        }
        public void ImprimirMatriz()
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    Console.WriteLine(matriz[j, i]);
                    
                }
            }
        }
    }
}
