﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leccion_17
{
    class Paralelos
    {
        private string[] nombres;
        private int[] edades;
        private int tamano = 5;

        public void CargarInfo()
        {
            nombres = new string[tamano];
            edades = new int[tamano];

            for(int i = 0; i < tamano; i++)
            {
                Console.WriteLine("Ingresa el nombre de la persona");
                nombres[i] = Console.ReadLine();

                Console.WriteLine("Ingresa la edad de la persona");
                edades[i] = int.Parse(Console.ReadLine());
            }          

        }
        public void DiferenciaDeEdad() 
        { 
            for(int i = 0; i < tamano; i++)
            {
                if(edades[i] >= 18)
                {
                    Console.WriteLine("La persona " + nombres[i] + " tiene " + edades[i] + " y es mayor de edad.");
                }

            }
        }

    }
}
