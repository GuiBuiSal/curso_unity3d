﻿using System;

namespace Leccion_17
{
    class Program
    {
        static void Main()
        {
            Ejercicio1();
        }
        public static void Ejercicio1()
        {
            //Desarrollar un programa que permita cargar 5 nombres de personas y sus edades respectivas. Luego de realizar la carga por teclado de todos los datos imprimir los nombres de las personas mayores de edad (mayores o iguales a 18 años)

            Paralelos ejercicio = new Paralelos();
            ejercicio.CargarInfo();
            ejercicio.DiferenciaDeEdad();

        }
    }
}
