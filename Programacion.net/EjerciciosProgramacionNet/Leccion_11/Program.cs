﻿using System;

namespace Leccion_11
{
    class Program
    {
        static void Main()
        {
            //Ejercicio1();
            Ejercicio2();
        }
        public static void Ejercicio1()
        {

            // Realizar un programa que acumule (sume) valores ingresados por teclado hasta ingresar el 9999 (no sumar dicho valor, indica que ha finalizado la carga). Imprimir el valor acumulado e informar si dicho valor es cero, mayor a cero o menor a cero. 
            int sumador = 0;
            int valor = 0;

            do
            {
                Console.WriteLine("Ingresa valores");
                valor = int.Parse(Console.ReadLine());

                if (valor != 9999)
                {
                    sumador = sumador + valor;
                }

            } while (valor != 9999);
            Console.WriteLine("La carga ha finalizado");
            if (sumador < 0)
            {
                Console.WriteLine("El valor acumulado es menor que cero: " + sumador);
            }
            if (sumador > 0)
            {
                Console.WriteLine("El valor acumulado es mayor que cero: " + sumador);
            }
            if (sumador == 0)
            {
                Console.WriteLine("El valor acumulado es igual que cero: " + sumador);
            }
        }
        public static void Ejercicio2()
        {
            /*En un banco se procesan datos de las cuentas corrientes de sus clientes. De cada cuenta corriente se conoce: número de cuenta y saldo actual. El ingreso de datos debe finalizar al ingresar un valor negativo en el número de cuenta.
                Se pide confeccionar un programa que lea los datos de las cuentas corrientes e informe:
                a)De cada cuenta: número de cuenta y estado de la cuenta según su saldo, sabiendo que:

                Estado de la cuenta	'Acreedor' si el saldo es >0.
                'Deudor' si el saldo es <0.
                'Nulo' si el saldo es =0.

                b) La suma total de los saldos acreedores. */
            int cuenta;
            int saldoActual;
            int sumaAcreedores = 0;

            do
            {
                Console.WriteLine("Ingrese el número de cuenta: ");
                cuenta = int.Parse(Console.ReadLine());

                if (cuenta >= 0)
                {
                    Console.WriteLine("Ingrese el saldo de la cuenta: ");
                    saldoActual = int.Parse(Console.ReadLine());

                    if (saldoActual > 0)
                    {
                        Console.WriteLine("Estado de la cuenta: Acreedor.");
                        sumaAcreedores = sumaAcreedores + saldoActual;
                    }
                    else if (saldoActual < 0) { Console.WriteLine("Estado de la cuenta: Deudor."); }
                    else { Console.WriteLine("Estado de la cuenta: Nulo."); }
                }

            } while (cuenta >= 0);

            Console.WriteLine("La suma total de los saldos de los acreedores es " + sumaAcreedores);

        }
    }
}
