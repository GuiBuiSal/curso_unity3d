﻿using System;

namespace Leccion_18
{
    class Program
    {
        static void Main()
        {
            Ejercicio1();
        }
        public static void Ejercicio1()
        {
            // Cargar un vector de n elementos. imprimir el menor y un mensaje si se repite dentro del vector.

            Conjunto mamotreto = new Conjunto();
            mamotreto.TamanoArray();
            mamotreto.CargarArray();
            mamotreto.Operaciones();
            mamotreto.Imprimir();

        }
    }
}
