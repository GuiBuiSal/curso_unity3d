﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leccion_18
{
    class Conjunto
    {
        // Cargar un vector de n elementos. imprimir el menor y un mensaje si se repite dentro del vector.
        private int[] cobetes;
        private int tamano = 0;
        private int menor = 0;
        private bool igual = false;
        private int poscicion = 0;

        public void TamanoArray()
        {
            Console.WriteLine("Ingrese el tamaño que desee del array");
            tamano = int.Parse(Console.ReadLine());
        }
        public void CargarArray() 
        {
            cobetes = new int[tamano];

            for(int i = 0; i < tamano; i++)
            {
                Console.WriteLine("Introduzca el valor para el elemento " + i + " del array");
                cobetes[i] = int.Parse(Console.ReadLine());
            }
        }
        public void Operaciones()
        {
            for(int i = 0; i < tamano; i++)
            {
                menor = cobetes[0];
                if(cobetes[i] < menor)
                {
                    menor = cobetes[i];
                    poscicion = i;
                }    
                if(menor == cobetes[i] && poscicion != i) { igual = true; }
            }
        }
        
        public void Imprimir()
        {
            Console.WriteLine("El menor valor de los elementos del array es " + menor);

           if(igual == true) { Console.WriteLine("El menor SÍ se repite en el array."); }
            else { Console.WriteLine("El menor NO se repite en el array."); }
        }

    }
}
