﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leccion_20
{
    class Paises
    {
        // Cargar en un vector los nombres de 5 paises y en otro vector paralelo la cantidad de habitantes del mismo. Ordenar alfabéticamente e imprimir los resultados. Por último ordenar con respecto a la cantidad de habitantes (de mayor a menor) e imprimir nuevamente.

        private string[] paises;
        private int[] habitantes;
        private int tamano = 5;
       
        

        public void CargarPaYHab() 
        {
            paises = new string[5];
            habitantes = new int[5];

            for(int i = 0; i < tamano; i++)
            {
                Console.WriteLine("Introduce el nombre del país:");
                paises[i] = Console.ReadLine();

                Console.WriteLine("Introduce los habitantes del país:");
                habitantes[i] = int.Parse(Console.ReadLine());
            }
        }
        public void OrdenarNombres()
        {
            Console.WriteLine("");
            Console.WriteLine("Países ordenados alfabéticamente:");
            for (int i = 0; i < paises.Length; i++)
            {
                for(int k = i+1; k <= paises.Length-1; k++)
                {
                    if (paises[i].CompareTo(paises[k]) > 0) // El compareTo lo que hace es comparar dos objetos del mismo tipo y devuelve un entero que indica                                         si la posición de la instancia actual es anterior, posterior o igual que la del otro objeto en el                                          criterio de ordenación.  ( public int CompareTo(object obj))
                    {
                        string alfabeto;

                        alfabeto = paises[k];
                        paises[k] = paises[i];
                        paises[i] = alfabeto;

                        int auxHabit = habitantes[k];
                        habitantes[k] = habitantes[i];
                        habitantes[i] = auxHabit;
                    }
                }

                Console.WriteLine(paises[i] + " " + habitantes[i]);
            }
            Console.WriteLine("");
        }
        public void OrdenarPorHabitante()
        {
            Console.WriteLine("");
            Console.WriteLine("Países ordenados por número de habitantes:");
            for (int i = 0; i < paises.Length; i++)
            {
                for (int k = i + 1; k <= paises.Length - 1; k++)
                {

                    if (habitantes[i] < habitantes[k])
                    {
                        int auxHabit = habitantes[k];
                        habitantes[k] = habitantes[i];
                        habitantes[i] = auxHabit;

                       string alfabeto = paises[k];
                        paises[k] = paises[i];
                        paises[i] = alfabeto;
                    }

                }
                Console.WriteLine(paises[i] + " " + habitantes[i]);

            }
        }
        public void Mostrar()
        {
            Console.WriteLine("La ordenación por nombre del país es: ");
            for (int i = 0; i < paises.Length; i++)
            {
               
               
            }
                
            Console.WriteLine("La ordenación por cantidad de habitantes en el país es: ");
            for (int i = 0; i < paises.Length; i++)
            {
                
            }
        }
    }
}
