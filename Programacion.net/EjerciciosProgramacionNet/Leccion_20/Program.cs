﻿using System;

namespace Leccion_20
{
    class Program
    {
        static void Main()
        {
            Ejercicio1();
        }
        public static void Ejercicio1()
        {
            // Cargar en un vector los nombres de 5 paises y en otro vector paralelo la cantidad de habitantes del mismo. Ordenar alfabéticamente e imprimir los resultados. Por último ordenar con respecto a la cantidad de habitantes (de mayor a menor) e imprimir nuevamente.

            Paises ordenamiento = new Paises();
            ordenamiento.CargarPaYHab();
            ordenamiento.OrdenarNombres();
            ordenamiento.OrdenarPorHabitante();
            //ordenamiento.Mostrar();

        }
    }
}
