﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leccion_15
{
    class Array
    {
        private int[] elementos;
        private int suma = 0;
        private int suma36 = 0;
        private int contador = 0;

        public void Ingresar()
        {
            elementos = new int[8];
            for (int i = 0; i < 8; i++)
            {
                Console.WriteLine("Ingrese el valor del elemento " + i);
                elementos[i] = int.Parse(Console.ReadLine());                          
            }            
        }
        public void SumarValor()
        {
            for (int i = 0; i < 8; i++)
            {
                suma = suma + elementos[i];
            }
        }

        public void SumaSi36()
        {
            for (int i = 0; i < 8; i++)
            {
                if (elementos[i] > 36)
                {
                    suma36 = suma36 + elementos[i];
                }
            }
        }
        public void Mayor50()
        {
            for (int i = 0; i < 8; i++)
            {
                if(elementos[i] > 50)
                {
                    contador = contador + 1;
                }
            }
        }
        public void Imprimir()
        {
            Console.WriteLine("El valor acumulado de todos los elementos del vector es " + suma);
            Console.WriteLine("El valor acumulado de los elementos del vector que son mayores a 36 es " + suma36);
            Console.WriteLine("La cantidad de valores mayores a 50 es " + contador);
        }
    }
}
