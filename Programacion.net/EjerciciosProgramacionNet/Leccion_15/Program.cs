﻿using System;
using System.Net.Http.Headers;

namespace Leccion_15
{
    class Program
    {
        static void Main()
        {
            //Ejercicio1();
            //Ejercicio2();
            //Ejercicio3();
            Ejercicio4();
        }
        public static void Ejercicio1()
        {

            /* Desarrollar un programa que permita ingresar un vector de 8 elementos, e informe:
               El valor acumulado de todos los elementos del vector.
               El valor acumulado de los elementos del vector que sean mayores a 36.
               Cantidad de valores mayores a 50.*/

            Array conjunto = new Array();

            conjunto.Ingresar();
            conjunto.SumarValor();
            conjunto.SumaSi36();
            conjunto.Mayor50();
            conjunto.Imprimir();
        }
        public static void Ejercicio2()
        {
            //Realizar un programa que pida la carga de dos vectores numéricos enteros de 4 elementos. Obtener la suma de los dos vectores, dicho resultado guardarlo en un tercer vector del mismo tamaño. Sumar componente a componente. 

            Vectores ejercicio2 = new Vectores();

            ejercicio2.IngresarVectores();
            ejercicio2.SumarComponentes();
            ejercicio2.Imprimir();
        }
        public static void Ejercicio3()
        {
            //Se tienen las notas del primer parcial de los alumnos de dos cursos, el curso A y el curso B, cada curso cuenta con 5 alumnos.

            //Realizar un programa que muestre el curso que obtuvo el mayor promedio general.
            Notas colegio = new Notas();
            colegio.IngresarNotas();
            colegio.Promedio();
            colegio.Impresion();
        }
        public static void Ejercicio4()
        {
            // Cargar un vector de 10 elementos y verificar posteriormente si el mismo está ordenado de menor a mayor.
            Ordenamiento vector = new Ordenamiento();
            vector.IngresarVectores();
            vector.VectorOrdenado();
            

        }

    }
}
