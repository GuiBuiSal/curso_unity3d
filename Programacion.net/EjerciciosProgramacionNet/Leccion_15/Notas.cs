﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leccion_15
{
    class Notas
    {
        private int[] claseA;
        private int[] claseB;
        private int sumaA = 0;
        private int sumaB = 0;
        private float mediaA = 0f;
        private float mediaB = 0f;
        private float alumnos = 5f;
        public void IngresarNotas()        
        {
            claseA = new int[5];
            claseB = new int[5];

            for(int i = 0; i < 5; i++)
            {
                Console.WriteLine("Introduzca la nota del alumno número " + (i+1) + " de la clase A");
                claseA[i] = int.Parse(Console.ReadLine());
            }
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Introduzca la nota del alumno número " + (i+1) + " de la clase B");
                claseB[i] = int.Parse(Console.ReadLine());
            }

        }
        public void Promedio()
        {
            for (int i = 0; i < 5; i++)
            {
                sumaA = sumaA + claseA[i];
            }
            for (int i = 0; i < 5; i++)
            {
                sumaB = sumaB + claseB[i];
            }

            mediaA = sumaA / alumnos;
            mediaB = sumaB / alumnos;
        }
        public void Impresion()
        {

            if (mediaA > mediaB) { Console.WriteLine("El curso que tuvo más promedio de notas fue el curso A con " + mediaA); }
            if (mediaA < mediaB) { Console.WriteLine("El curso que tuvo más promedio de notas fue el curso B con " + mediaB); }
        }
    }
}
