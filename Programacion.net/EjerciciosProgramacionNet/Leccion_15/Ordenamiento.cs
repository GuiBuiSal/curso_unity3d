﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Leccion_15
{
    class Ordenamiento
    {
        private int[] vector;
        private bool orden = true;

        public void IngresarVectores()
        {
            vector = new int[10];
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Ingrese el valor del elemento " + i + " del array");
                vector[i] = int.Parse(Console.ReadLine());
            }

        }
        public void VectorOrdenado()
        {
            for (int i = 0; i < 9; i++)
            {
                if (vector[i+1] < vector[i]) 
                { 
                    orden = false; 
                }                
            }
            if (orden == true) { Console.WriteLine("El vector SÍ está ordenado de menor a mayor"); }
            else { Console.WriteLine("El vector está NO ordenado de menor a mayor"); }
        }
        
    }
   
}
