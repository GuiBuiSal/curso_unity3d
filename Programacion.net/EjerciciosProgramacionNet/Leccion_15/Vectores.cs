﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leccion_15
{
    class Vectores
    {
        private int[] vector1;
        private int[] vector2;
        private int[] vectorFinal;

        public void IngresarVectores()
        {
            vector1 = new int[4];
            vector2 = new int[4];
            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine("Ingrese el valor del elemento " + i + " del array 1");
                vector1[i] = int.Parse(Console.ReadLine());
            }
            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine("Ingrese el valor del elemento " + i + " del array 2");
                vector2[i] = int.Parse(Console.ReadLine());
            }
        }
        public void SumarComponentes()
        {
            vectorFinal = new int [4];
            for (int i = 0; i < 4; i++)
            {
                vectorFinal[i] = vector1[i] + vector2[i];
            }
        }
        public void Imprimir()
        {
            for (int i = 0; i < 4; i++)
            {
               Console.WriteLine(vector1[i]);
            }
            Console.WriteLine();
            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine(vector2[i]);
            }
            Console.WriteLine("El resultado del Vector final es: ");
            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine(vectorFinal[i]);
            }
        }
    }
}
