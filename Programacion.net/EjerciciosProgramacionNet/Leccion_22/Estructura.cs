﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leccion_22
{
    
    class Estructura
    {
        private int[,] matrioska;
        int f = 0;
        int c = 0;

        public void TamanoYCarga()
        {
            Console.WriteLine("inserte el número de filas:");
            f = int.Parse(Console.ReadLine());

            Console.WriteLine("inserte el número de columnas:");
            c = int.Parse(Console.ReadLine());
            matrioska = new int[f,c];

            for(int f = 0; f < matrioska.GetLength(0); f++)
            {
                for(int c = 0; c < matrioska.GetLength(1); c++)
                {
                    Console.WriteLine("Ingresa el valor del componente");
                    matrioska[f, c] = int.Parse(Console.ReadLine());
                }
            }
            Console.WriteLine("");
        }
        public void ImprimirAntes()
        {
            for (int f = 0; f < matrioska.GetLength(0); f++)
            {
                for (int c = 0; c < matrioska.GetLength(1); c++)
                {
                    Console.WriteLine(matrioska[f, c]);
                    
                }
            }
            Console.WriteLine("");
        }
        public void IntercambiarLineas()
        {
            
            
                for(int c = 0; c < matrioska.GetLength(1); c++)
                {
                  
                    int varAux = matrioska[0, c];
                    matrioska[0, c] = matrioska[1, c];
                    matrioska[1, c] = varAux;
                }
            
            Console.WriteLine("");
        }
        public void ImprimirDespues()
        {
            for (int f = 0; f < matrioska.GetLength(0); f++)
            {
                for (int c = 0; c < matrioska.GetLength(1); c++)
                {
                    Console.WriteLine(matrioska[f, c]);

                }
            }
        }
        public void imprimirEsquinas()
        {
            
                
                    Console.WriteLine("El vértice que se muestra es el superior izquierdo");
                    Console.WriteLine(matrioska[0, 0]);
                    Console.WriteLine("El vértice que se muestra es el superior derecho");
                    Console.WriteLine(matrioska[0, matrioska.GetLength(1) - 1]);
                    Console.WriteLine("El vértice que se muestra es el inferior izquierdo");
                    Console.WriteLine(matrioska[matrioska.GetLength(0) - 1, 0]);
                    Console.WriteLine("El vértice que se muestra es el inferior derecho");
                    Console.WriteLine(matrioska[matrioska.GetLength(0) - 1, matrioska.GetLength(1) - 1]);
                
        }


    }
}
