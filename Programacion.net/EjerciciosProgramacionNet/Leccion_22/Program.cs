﻿using System;

namespace Leccion_22
{
    class Program
    {
        static void Main()
        {
            //Ejercicio1();
            Ejercicio2();
        }

        public static void Ejercicio1() 
        {
            // Crear una matriz de n * m filas (cargar n y m por teclado) Intercambiar la primer fila con la segundo. Imprimir luego la matriz. 

            Estructura operation = new Estructura();

            operation.TamanoYCarga();
            operation.ImprimirAntes();
            operation.IntercambiarLineas();
            operation.ImprimirDespues();

        }
        public static void Ejercicio2()
        {
            // Crear una matriz de n* m filas(cargar n y m por teclado) Imprimir los cuatro valores que se encuentran en los vértices de la misma(mat[0][0] etc.)
            
            Estructura operation = new Estructura();

            operation.TamanoYCarga();
            operation.imprimirEsquinas();
        }
    }
}
