﻿using System;

namespace Leccion_23
{
    class Program
    {
        static void Main()
        {
            /* Se desea saber la temperatura media trimestral de cuatro paises. Para ello se tiene como dato las temperaturas medias mensuales de dichos paises.
               Se debe ingresar el nombre del país y seguidamente las tres temperaturas medias mensuales.
               Seleccionar las estructuras de datos adecuadas para el almacenamiento de los datos en memoria.
                    a - Cargar por teclado los nombres de los paises y las temperaturas medias mensuales.
                    b - Imprimir los nombres de las paises y las temperaturas medias mensuales de las mismas.
                    c - Calcular la temperatura media trimestral de cada país.
                    c - Imprimr los nombres de las provincias y las temperaturas medias trimestrales.
                    b - Imprimir el nombre de la provincia con la temperatura media trimestral mayor.  */

            Operaciones temps = new Operaciones();
            temps.CargarDatos();
            temps.ImprimirNomYTempMens();
            temps.ImprimirMediasPaisTri();
        }
    }
}
