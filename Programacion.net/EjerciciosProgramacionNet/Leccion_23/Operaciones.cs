﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leccion_23
{
    class Operaciones
    {

        /* Se desea saber la temperatura media trimestral de cuatro paises. Para ello se tiene como dato las temperaturas medias mensuales de dichos paises.
           Se debe ingresar el nombre del país y seguidamente las tres temperaturas medias mensuales.
           Seleccionar las estructuras de datos adecuadas para el almacenamiento de los datos en memoria.
                a - Cargar por teclado los nombres de los paises y las temperaturas medias mensuales.
                b - Imprimir los nombres de las paises y las temperaturas medias mensuales de las mismas.
                c - Calcular la temperatura media trimestral de cada país.
                c - Imprimr los nombres de las provincias y las temperaturas medias trimestrales.
                b - Imprimir el nombre de la provincia con la temperatura media trimestral mayor.  */

        float [,]tempMediaMensual;
        float[] temptri;
        string[] paises;
        


        public void CargarDatos()
        {
            tempMediaMensual = new float[4, 12];
            paises = new string[4];

            for (int i = 0; i < paises.Length; i++)
            {
                Console.WriteLine("Ingrese el nombre del país en la posición " + (i + 1));
                paises[i] = Console.ReadLine();
            }

            for(int f = 0; f < tempMediaMensual.GetLength(0); f++)
            {
                for (int c = 0; c < tempMediaMensual.GetLength(1); c++)
                {
                    Console.WriteLine("Ingrese el valor en la posición " + f + "," + c);
                    tempMediaMensual[f,c] = float.Parse(Console.ReadLine());
                }
            }

        } // Fin de CargarDatos()

        public void ImprimirNomYTempMens()
        {
            for (int i = 0; i < paises.Length; i++)
            {
                Console.WriteLine(paises[i]);
                
            }
            Console.WriteLine("");

            for (int f = 0; f < tempMediaMensual.GetLength(0); f++)
            {
                for (int c = 0; c < tempMediaMensual.GetLength(1); c++)
                {
                    Console.WriteLine(tempMediaMensual[f, c]);
                    
                }
            }
        } // Fin de ImprimirNomYTempMens()

        public void CalcularMediasTrimestrales()
        {
            temptri = new float[4];
            for (int f = 0; f < tempMediaMensual.GetLength(0); f++)
            {
                float suma = 0;
                for (int c = 0; c < tempMediaMensual.GetLength(1); c++)
                {
                    suma = suma + tempMediaMensual[f, c];
                }
                temptri[f] = suma / 3;
            }
        } // Fin de CalcularMediasTrimestrales()
        public void ImprimirMediasPaisTri()
        {
            Console.WriteLine("Temperaturas trimestrales.");
            for (int f = 0; f < paises.Length; f++)
            {
                Console.WriteLine(paises[f] + " " + temptri[f]);
            }

        }
    

       
    }
}
