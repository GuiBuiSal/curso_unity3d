﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leccion_14
{
    // Confeccionar una clase que permita ingresar valores enteros por teclado y nos muestre la tabla de multiplicar de dicho valor. Finalizar el programa al ingresar el -1.

    class Math
    {
        private int valorEntero;
        public void CargarEnteros()
        {
            do 
            {
                Console.WriteLine("Introduzca un valor entero");
                valorEntero = int.Parse(Console.ReadLine());
                if (valorEntero != -1)
                {
                    for (int i = 1; i <= 10; i++)
                    {
                        Console.Write(valorEntero * i + " - ");                        
                    }
                }
               
            }while(valorEntero !=-1);
        }
    }
}
