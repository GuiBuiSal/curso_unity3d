﻿using System;

namespace Leccion_14
{
    class Program
    {
        static void Main()
        {
            //Ejercicio1();
            Ejercicio2();
        }
        public static void Ejercicio1()
        {
            // Confeccionar una clase que permita ingresar valores enteros por teclado y nos muestre la tabla de multiplicar de dicho valor. Finalizar el programa al ingresar el -1.

            Math operaciones = new Math();
            operaciones.CargarEnteros();
        }
        public static void Ejercicio2()
        {
            // Confeccionar una clase que permita ingresar tres valores por teclado. Luego mostrar el mayor y el menor.

            Valores cosas = new Valores();
            cosas.IngresarValores();
            cosas.Comparar();
            
            cosas.Mostrar();
        }


    }
}
