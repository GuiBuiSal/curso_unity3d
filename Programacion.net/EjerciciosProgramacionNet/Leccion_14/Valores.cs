﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leccion_14
{
    class Valores
    {
        private int num1;
        private int num2;
        private int num3;
        private int mayor;
        private int menor;

        public void IngresarValores()
        {
            Console.WriteLine("Ingrese el primer valor:");
            num1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Ingrese el segundo valor:");
            num2 = int.Parse(Console.ReadLine());

            Console.WriteLine("Ingrese el tercer valor:");
            num3 = int.Parse(Console.ReadLine());
        }
        public void Comparar()
        {
            if (num1 > num2 && num1 > num3) 
            {
                mayor = num1;
                if (num2 < num3) { menor = num2; }
                else { menor = num3; }
            }
            else if (num2 > num1 && num2 > num3) 
            {
                mayor = num2;
                if (num1 < num3) { menor = num1; }
                else { menor = num3; }
            }
            else if (num3 > num1 && num3 > num2) 
            { 
                mayor = num3;
                if (num1 < num2) { menor = num1; }
                else { menor = num2; }
            }
            
        }
       
        public void Mostrar()
        {
            Console.WriteLine("El número mayor es: " + mayor);
            Console.WriteLine("El número menor es: " + menor);
        }
           
    }
}
