﻿using System;

// Mini juego
// fase 1: Como jugadores tenemos una unidad y luchamos contra un enemigo
namespace Leccion_13_EjemploPOO
{
    class Unidad
    {
        public String nombre;
        public int vida;
        public int ataque;
        public int pocion;
        // Los constructores: Son métodos especiales para construir,No devuelven nada concreto, solo un objeto del tipo de la propia clase
        // Constructor por defecto ( Unidad())
        public Unidad()
        {
            this.nombre = "Unidad sin nombre";
            this.vida = 100;
            this.ataque = 10;
            this.pocion = 20;
        }

        // Constructor por parámetros
        public Unidad(String nombre, int nuevaVida, int ataque) 
        {
            this.nombre = nombre;
            this.vida = nuevaVida;
            this.ataque = ataque;
        }
        public Unidad(String nombre,int ataque)
        {
            this.nombre = nombre;
            this.vida = 100;
            this.ataque = ataque;
        }
       public string EnTexto()
        {
            return "\nNombre: " + this.nombre + ", Vida/Ataque " + this.vida + " / " + this.ataque;
        }
        public void Mostrar(String tipoDeUnidad)
        {
            Console.WriteLine("Unidad " + tipoDeUnidad + ": " + this.nombre);
            Console.WriteLine("Vida/ataque " + this.vida + " / " + this.ataque);
                       
        }

        // Crear un método que quite x vida(parámetro) a la vida del objeto(variable miembro).
        public void QuitarVida(int ataque)
        {
            this.vida -= ataque;
        }

        public void Atacar(Unidad uniQueRecibe) //Una unidad que recibe el daño (Forma 2 de quitar vida)
        {
            uniQueRecibe.vida = uniQueRecibe.vida - this.ataque;
        }
        public void Curar() 
        {
            this.vida += pocion;
        }
    }

    class Juego
    {
        public Unidad jugador = null;
        public Unidad enemigo = null;
        public Unidad enemigo11 = null;
        

        public void Inicializar()
        {
            this.enemigo = new Unidad("Cthulu", 50, 10);
            this.enemigo11 = new Unidad("Yogui", 30);
            
            this.jugador = new Unidad();
            Console.WriteLine("Di tu nombre");
            this.jugador.nombre = Console.ReadLine();
            Console.WriteLine("Indica tu ataque");
            this.jugador.ataque = int.Parse(Console.ReadLine());
            this.jugador.vida = 50;
           
        }
        public void MostrarUnidades()
        {
            jugador.Mostrar("JUGADOR");
            Console.WriteLine(jugador.EnTexto());
            enemigo.Mostrar("ENEMIGO");
            Console.WriteLine(jugador.EnTexto());
        }

        // Un método comenzarAtaques() que haga que enemigo ataque a jugador y jugador a enemigo, usando el metodo anterior de quitar vida
        public void ComenzarAtaques()
        {
            jugador.QuitarVida(enemigo.ataque);
            enemigo.QuitarVida(jugador.ataque);
        }
        public void ContinuarAtaques() // Forma 2 de quitar vida
        {
            enemigo.Atacar(this.jugador);
            jugador.Atacar(this.enemigo);
        }
    }

    // La clase Program la hacemos static para que no se pueda instanciar como un objeto.
    static class Program
    {

       
        static void Main()
        {
            Console.WriteLine("Juego!");
            Juego miJuego = new Juego();
            miJuego.Inicializar();
            miJuego.MostrarUnidades();
            miJuego.ComenzarAtaques();
        }
    }
}
