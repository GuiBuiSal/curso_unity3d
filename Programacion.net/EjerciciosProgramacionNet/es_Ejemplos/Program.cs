﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaWhile8
{
    class Program
    {
        static void Main(string[] args)
        {
            int repeticion, termino;
            repeticion = 1;
            termino = 11;
            while (repeticion <= 25)
            {
                Console.Write(termino);
                Console.Write(" - ");
                repeticion = repeticion + 1;
                termino = termino + 11;
            }
            Console.ReadKey();
        }
    }
}