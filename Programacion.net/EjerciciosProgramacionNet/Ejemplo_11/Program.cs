﻿using System;
using System.Collections.Generic;

//    

namespace Leccion11_Ejemplo
{
    class Program
    {
        // Ahora lo que hace es leer el array y meter en dos listas los strings que 
        // contienen Clone  y los que no, usando  Contains()
        // Ejercicio: Similar, meter en dos listas los de 1 y los de 2 usando IndexOf()
        static void Main(string[] args)
        {
            string[] nombresEnemigos =
            {
                "Lata 1",           // Objetos de la escena precargados
                "Lata 2 (Clone)", // Objetos de la escena instanciados por código, osea, dinámicos
                "Lata 2 (Clone)",
                "Lata 2 (Clone)",
                "Lata 2",
                "Basura 1",
                "Basura 2"
            };

            string dosPrimeros = "Los dos primeros enemigos son " + nombresEnemigos[0] + ", " + nombresEnemigos[1];
            Console.WriteLine(dosPrimeros);
            List<string> obj_2 = new List<string>();
            List<string> obj_1 = new List<string>();

            for (int i = 0; i < nombresEnemigos.Length; i++)
            {
                string enemigoActual = nombresEnemigos[i];
                if (enemigoActual.IndexOf("1") >= 0) 
                {
                    obj_1.Add(enemigoActual);
                }
                else
                {
                    obj_2.Add(enemigoActual);
                }
            }
            Console.WriteLine("Objetos con 1: " + obj_1.Count);
            for (int i = 0; i < obj_1.Count; i++)
            {
                string enemigoActual = obj_1[i];
                Console.WriteLine("Objetos con 1: " + enemigoActual);
            }
            Console.WriteLine("Objetos con 2: " + obj_2.Count);
            foreach (string enemAc in obj_2)
            {
                Console.WriteLine("Objetos con 2: " + enemAc);
            }
            Console.WriteLine("Hola que pasa".IndexOf("Nada"));
            Console.WriteLine("Hola que pasa".IndexOf("Hola"));
            Console.WriteLine("Hola que pasa".IndexOf("que"));

        }
    }
}
