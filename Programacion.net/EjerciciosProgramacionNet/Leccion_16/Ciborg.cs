﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leccion_16
{
    class Ciborg
    {
        private int n = 0;
        private int[] elementos;
        public void CuantosN()
        {
            Console.WriteLine("Introduzca el tamaño que desea para el array");
            n = int.Parse(Console.ReadLine());
        }
        public void LlenarArray()
        {
            elementos = new int[n];

            for(int i = 0; i < n; i++)
            {
                Console.WriteLine("Introduzca el valor del elemento " + i + " del array" );
                 elementos[i]= int.Parse(Console.ReadLine());
            }
        }
        public void Imprimir() 
        {
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("El valor de la posición " + i + " del array es " + elementos[i]);
            }
        }
    }
}
