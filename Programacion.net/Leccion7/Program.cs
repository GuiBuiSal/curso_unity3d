﻿using System;

namespace Leccion7
{
    class Program
    {
        static void Main()
        {
            //Ejercicio1();
            //Ejercicio2();
            //Ejercicio3();
            Ejercicio4();

        }
        public static void Ejercicio1()
        {
            string numeroUno;
            string numeroDos;
            string numeroTres;

            Console.WriteLine("Introduce el primer número: ");
            numeroUno = Console.ReadLine();
            int num1 = int.Parse(numeroUno);

            Console.WriteLine("Introduce el segundo número: ");
            numeroDos = Console.ReadLine();
            int num2 = int.Parse(numeroDos);

            Console.WriteLine("Introduce el tercer número: ");
            numeroTres = Console.ReadLine();
            int num3 = int.Parse(numeroTres);

            if (num1 > num2 && num1 > num3) { Console.WriteLine("El numero mayor es el primer número el " + num1); }
            else if (num2 > num1 && num2 > num3) { Console.WriteLine("El numero mayor es el segundo número el " + num2); }
            else { Console.WriteLine("El numero mayor es el tercer número el " + num3); }
        }
        public static void Ejercicio2()
        {
            // Se ingresa por teclado un valor entero, mostrar una leyenda que indique si el número es positivo, nulo o negativo. 

            string numero;
            Console.WriteLine("Introduce un número: ");
            numero = Console.ReadLine();
            int num = int.Parse(numero);
            if(num == 0) { Console.WriteLine("El número " + num + " es nulo."); }
            else if(num < 0) { Console.WriteLine("El número " + num + " es negativo."); }
            else { Console.WriteLine("El número " + num + " es positivo."); }
        }
        public static void Ejercicio3()
        {
            //Confeccionar un programa que permita cargar un número entero positivo de hasta tres cifras y muestre un mensaje indicando si tiene 1, 2, o 3 cifras. Mostrar un mensaje de error si el número de cifras es mayor. 

            string numero;
            Console.WriteLine("Introduce un número: ");
            numero = Console.ReadLine();
            int num = int.Parse(numero);
            if (num < 10 && num >= 0) { Console.WriteLine("El número tiene una cifra: " + num); }
            else if (num >= 10 && num <= 99) { Console.WriteLine("El número tiene dos cifras: " + num); }
            else if(num >= 100 && num <= 999) { Console.WriteLine("El número tiene tres cifras: " + num); }
            else { Console.WriteLine("Error el número: " + num + " está fuera de los parámetros"); }
        }
        public static void Ejercicio4()
        {
            /* Un postulante a un empleo, realiza un test de capacitación, se obtuvo la siguiente información: cantidad total de preguntas que se le realizaron y la cantidad de preguntas que contestó correctamente. Se pide confeccionar un programa que ingrese los dos datos por teclado e informe el nivel del mismo según el porcentaje de respuestas correctas que ha obtenido, y sabiendo que:

               Nivel máximo:	Porcentaje >= 90 %.

               Nivel medio:	Porcentaje >= 75 % y < 90 %.

               Nivel regular:	Porcentaje >= 50 % y < 75 %.

               Fuera de nivel: Porcentaje < 50 %.*/

            string totalPreguntas;
            Console.WriteLine("Introduce el número total de preguntas: ");
            totalPreguntas = Console.ReadLine();
            float NumTotalPreguntas = float.Parse(totalPreguntas);

            string preguntasAcertadas;
            Console.WriteLine("Introduce el número de preguntas acertadas: ");
            preguntasAcertadas = Console.ReadLine();
            float numPreguntasAcertadas = float.Parse(preguntasAcertadas);

            float porcentaje = (numPreguntasAcertadas * 100) / NumTotalPreguntas;

            if(porcentaje >= 90) { Console.WriteLine("El postulante al empleo tiene una capacitación del " + porcentaje + "%. Tiene el nivel máximo"); }
            else if (porcentaje < 90 && porcentaje >= 75) { Console.WriteLine("El postulante al empleo tiene una capacitación del " + porcentaje + "%. Tiene un nivel medio"); }
            else if (porcentaje < 75 && porcentaje >= 50) { Console.WriteLine("El postulante al empleo tiene una capacitación del " + porcentaje + "%. Tiene un nivel regular"); }
            else { Console.WriteLine("El postulante al empleo tiene una capacitación del " + porcentaje + "%. Está fuera de nivel"); }
        }
    }
}
