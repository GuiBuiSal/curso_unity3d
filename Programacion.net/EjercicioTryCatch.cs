using System;

public class EjercicioTryCatch
{
	static void Main()
	{
		// Provocar un error de límites de array. Es decir, creamos un array de 5 elementos 
		//e intentamos acceder al 7º(que no existe, provocará un error). Luego controlarlo con try/catch
		
		int[] numeros = {0,1,2,3,4};
		
		try
		{
			Console.WriteLine ("Comienzo de búsqueda de errores");
			Console.WriteLine(numeros[7]);
		}
		catch (FormatException error)
		{
			Console.WriteLine("Error de formato");
			Console.WriteLine(error.Message);
			
		}
		catch (IndexOutOfRangeException error)
		{
			Console.WriteLine("Error de array");
			Console.WriteLine(error.Message);
			Console.WriteLine("Elige una posición dentro del rango declarado (del 0 al 4)");
		}
		catch (Exception error)
		{
			Console.WriteLine("Error genérico");
			Console.WriteLine(error.Message);
			
		}
		
		Console.WriteLine("Final de la búsqueda de errores");
	}
	
}