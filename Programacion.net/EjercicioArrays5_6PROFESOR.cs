using System;

	// Ejercicio 5: Crear una función que calcule la media de todos los ataques
	// Ejercicio 6: Crear una función que calcule el mínimo de los ataques de los enemigos CERCANOS.
	// Ejercicio 7: Crear una función que devuelva un array que contenga el doble de los ataques:  { 1.6f, 0.85f, ....}
	
public class EjemploArrays5_6 
{	
	public static void Main() 
	{
		EjercicioArrayAtaques(); 
	}	
	public static void EjercicioArrayAtaques() 
	{
		float[] ataques = {3.2f, 1.7f, 2.4f, 5.0f, 7.1f, 4.8f};
		bool[] siCercanos = {false, true, true, false, true, true};
		
		// Ejercicio 5: Mostrar ataque medio
		float mediaAtaques;
		mediaAtaques = CalcMediaAtaques( ataques );
		Console.WriteLine("La media de ataques es = " + mediaAtaques);
		// Ejercicio 6: Crear una función que calcule el mínimo de los ataques de los enemigos CERCANOS.
		float ataqueMin;
		ataqueMin = CalcCercanosMinimo(ataques[],siCercanos[]);
		Console.WriteLine("El ataque mínimo de los enemigos cercanos es: " + ataqueMin);
		
	}
	public static float CalcMediaAtaques(float[] ataques)
	{
		float sumaAtaques= 0f;
		float mediaAtaques = 0f;
		
		for (int i = 0; i< ataques.Length; i +=1)
		{sumaAtaques = sumaAtaques + ataques[i];}
		
		mediaAtaques = sumaAtaques / ataques.Length;
		return mediaAtaques;		
	}
	public static float CalcCercanosMinimo(float[] ataques, bool[] siCercanos)
	// Ejercicio 6: Crear una función que calcule el mínimo de los ataques de los enemigos CERCANOS.
	{
		float ataqueMin = ataque[i];
		for (i = 0; i< ataques.Length; i++)
		{
			if(siCercanos[i] == true)
			{
				ataqueMin < ataques[i];
				ataqueMin = ataques[i];
			}
		}
		return ataqueMin;
	}
}