using System;

public class EjemploTryCatch
{
	static void Main()
	{
		try
		{
			Console.WriteLine ("Empieza el programa");
		int x = Int32.Parse("No es num");  // Lanzará una excepción
		Console.WriteLine ("Esta línea no se ejecuta");
		
		}catch(Exception error)
		{
			Console.WriteLine("Pero al capturar el error, sí permitimos comtinuar con el programa");
			Console.WriteLine("Incluso mostrar el error de manera controlada: ");
			Console.WriteLine(error.Message);
			
		}
		Console.WriteLine("Termina el programa");
		
	}
}