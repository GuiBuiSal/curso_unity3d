﻿using System;

struct Jugador
{
    public string nombre;
    public int edad;
    public bool haPagado;  // true ha pagado, false es free to play
    public float vida;

    // Constructor: Función especial que sirve para incializar 
    // los datos de una estructura del tirón. En las estructuras es obligatorio
    // inicializar todos los variables miembro (campos, propiedades, atributos...)
    public Jugador (string n, int edad, bool haPagado)
    {
        nombre = n;
        this.edad = edad;
        this.haPagado = haPagado;
        this.vida = 100;
    }

    public void Mostrar()   // Debe ser público para poder invocarlo (llamarlo)
    {
        Console.WriteLine("Jugador: " + this.nombre
            + " vida = " + vida 
            + ( this.haPagado ? "  VIP" : "  Free To Play"));
    }
}
// Estructura enemigo: nombre, ataque (float) 
struct Enemigo
{
    public string nombre;
    public float ataque;

    public Enemigo(string nombre, float ataque)
    {
        this.nombre = nombre;
        this.ataque = ataque;
    }

    public void Mostrar()
    {
        Console.WriteLine("Enemigo: " + this.nombre + " ataque = " + ataque);
    }
    public string MostrarConAtaque(string nombreDelAtaque)
    {
        string texto = "Enemigo: " + this.nombre + " " + nombreDelAtaque + " = " + ataque;
        Console.WriteLine(texto);
        return texto;
    }
    public void Ataque(ref Jugador jug)
    {
        jug.vida = jug.vida - this.ataque;
        Console.WriteLine("Acaba de hacer el ataque. Resultado:");
        jug.Mostrar();
    }
    public void Ataque(ref float vida)
    {
        vida = vida - this.ataque;
        Console.WriteLine("Acaba de quitar vida:" + vida);
    }
    // No 
    public void AtaqueAVarios(ref Jugador[] jugadores)
    {
        for (int i = 0; i < jugadores.Length; i++)
        {
            jugadores[i].vida = jugadores[i].vida - this.ataque;
            jugadores[i].Mostrar();
        }
    }
}

public class Class1
{
    static public void Main()
    {
        Jugador jug = new Jugador("Fulano", 30, false);
        jug.Mostrar();
        //MostrarPersona(jug.nombre, jug.edad, jug.haPagado);

        Jugador jug2 = new Jugador("Fulanita", 32, true);
        jug2.Mostrar();

        Enemigo enemigo = new Enemigo("Cthulhu", 10);
        enemigo.Mostrar();
        enemigo.MostrarConAtaque("Hechizo");

        Enemigo enemigo2 = new Enemigo("Covid-19", 20);
        enemigo.Mostrar();

        enemigo.Ataque(ref jug);    // DOBLE ATAQUE
        enemigo.Ataque(ref jug.vida);
        enemigo.Ataque(ref jug);    // TRIPLE ATAQUE

        // enemigo2.Ataque(jug);
        // enemigo2.Ataque(jug2);
        Jugador[] todosLosJug = new  Jugador[2];
        todosLosJug[0] = jug;
        todosLosJug[1] = jug2;
        enemigo2.AtaqueAVarios(ref todosLosJug);    // DOBLE ATAQUE
        enemigo2.AtaqueAVarios(ref todosLosJug);

        Console.WriteLine("RESULTADOS FINALES ");
        jug.Mostrar();  // 40
        jug2.Mostrar(); // 60
        todosLosJug[0].Mostrar();

        // 1 - Crear otro enemigo. 
        // 2 - Añadir campo vida de tipo float al jugador.
        // 3 - Hacer un método Ataque en enemigo que reciba un jugador como parámetro, 
        //     y que le quite vida
        // 4 - Usarlos: 
        //      4.1: enem_1 ataca al jug1, 
        //      4.2: y enem2 ataca a todos los jugadores    
    }
}
