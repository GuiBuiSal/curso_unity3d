using System;

public class EjemploArrays 
{	
	public static void Main() 
	{
		EjercicioArrayAtaques(); 
	}
	
	public static void EjercicioArrayAtaques() 
	{
		float[] ataques = {3.2f, 1.7f, 2.4f, 5.0f, 7.1f, 4.8f};
		bool[] siCercanos = {false, true, true, false, true, true};
		
		float totalAtaques;
		totalAtaques = CalcAtaqueTotalCercanos( ataques, siCercanos );
				
		bool[] siCercanosJug2 = {true, true, false, false, true, false};
		float totalAtqJug2 = CalcAtaqueTotalCercanos( ataques, siCercanosJug2 );
		
		Console.WriteLine("Total = " + totalAtaques);
		Console.WriteLine("Total = " + totalAtqJug2);
	}
	
	public static float CalcAtaqueTotalCercanos(float[] ataques, bool[] siCercanos)
	{		
		float total;
		
		total = 0;	// Inicializar variables!!
		
		// Para cuando los ataques sumen sean los verdaderos
		for (int i = 0; i < ataques.Length; i = i + 1) 
		{
			Console.WriteLine("Valor de " + i + " es " + ataques[i]);
			if (siCercanos[i]) {
				total = total + ataques[i];
			}
		}
		return total;
	}
	// Ejercicio 2: Generar un array con textos con la info de los NO cercanos:
	// El resultado es un array: {"Enemigo 0: 3.2", "Enemigo 3: 5.0f"}
	
	// Ejercicio 3: Crear una función que calcule el ataque total de los que tengan ataque > 3
	// 				este tope debe pasarse por parámetro
	
	// Ejercicio 4: Otra que devuelva el ataque máximo del array
}




