﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo_Clases
{
    class Progresion
    {
        public int x;
        public int y;

        public void CargarDatos()
        {
            Console.WriteLine("Introduzca el número que se va a multiplicar:");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca cuántas veces se va a repetir el bucle:");
            y = int.Parse(Console.ReadLine());
        }
        public void GenerarSerie()
        {
            Console.WriteLine("");
            Console.WriteLine(y + " veces " + x);
            
            for (int contador = 1; contador <= y; contador++)
            {
                Console.Write(contador * x + " - ");                
            }
        }
       /* public void MostrarSerie()
        {
            for (int contador = 1; contador <= x; contador++)
                Console.Write(contador * y + " - ");
        }*/
    }
}
