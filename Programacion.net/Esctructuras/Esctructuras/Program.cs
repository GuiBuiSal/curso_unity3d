﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esctructuras
{
    class Program
    {
        static void Main()
        {
            string nombre = "Fulano";
            int edad = 30;
            bool fumador = false;

            MostrarPersona(nombre, edad, fumador);

            string nombre_2 = "Fulanita";
            int edad_2 = 32;
            bool fumador_2 = true;

            MostrarPersona(nombre_2, edad_2, fumador_2);

            string nombre_3 = "Francisquete";
            int edad_3 = 45;
            bool fumador_3 = true;

            MostrarPersona(nombre_3, edad_3, fumador_3);
        }

        static void MostrarPersona(string n, int e, bool f)
        {
            Console.WriteLine("Persona: " + n + " edad = " + e + e + (f ? " Fuma" : " No fuma"));
        }
    }
}
