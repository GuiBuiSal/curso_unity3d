using System;
/* Estos son los tipos primitivos de C# (igual que en otros lenguaje de programaci?n)

*/
public class TiposDeDatos 
{

 static void Main()
{
		byte unByte = 255;  // N? entero entre 0 y 255, 8 bits

		char unCaracter = 'A';  // Se refiere a un caracter o bien 1 o 2 bytes

		int numEntero = 1000; // N?mero entre -2.000.000.000 y + 2.000.000.000
								// porque ocupa 4 bytes. 2^32 0 4.000.000.000
								
		Console.WriteLine("Byte: " + unByte + "char : " + unCaracter);
		Console.WriteLine("Un char ocupa " + sizeof(char) + " bytes");
		Console.WriteLine("El entero vale " + numEntero);
		Console.WriteLine("Y ocupa " + sizeof(int) + " bytes");
		numEntero = 1000000000;
		Console.WriteLine("Ahora el entero vale " + numEntero);
		
		// para guardar n?mero m?s largos
		long enteroLargo = 5000000000L;  //ocupa 8 bytes
		Console.WriteLine("Entero largo vale " + enteroLargo);
	
		// Tipos de decimales: float (4bytes) y double (8bytes)
		// Precisi?n es de :  7-8 cifras en TOTAL   15-16 cifras en TOTAL
		// Por defecto detecta el double, si es float hay que a?adir una f
	
		float numDecimal = 1.23456789f;
		Console.WriteLine("Num decimal float vale " + numDecimal);
		
		// Para m?s precisi?n: double
		double numDoblePrecision = 12345.6789123456789;
		Console.WriteLine("Num decimal doble vale " + numDoblePrecision);
		
		// Para guardar Si/no, Verdad/Falso, Cero/uno...
		bool variableBooleana = true; // 1 ?nico byte
		Console.WriteLine("variable Booleana vale " + variableBooleana);
		// Podemos guardar comparaciones, condiciones, etc
		variableBooleana = numDecimal > 1000;
		Console.WriteLine("variable Booleana ahora es " + variableBooleana);
		variableBooleana = numDecimal < 1000;
		Console.WriteLine("variable Booleana ahora es " + variableBooleana);
		
		string cadenaDeTexto = "Una cadena de texto";
		
		Console.WriteLine(cadenaDeTexto); 
		Console.WriteLine(cadenaDeTexto + " que " + "permite concatenacion");
		
		// Lo que no permite son conversiones inv?lidas:
		int otroNum = 1;
		string otroTexto = "" + numEntero;
}
}		