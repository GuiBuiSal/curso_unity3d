﻿using System;
// Mini juego: 
// Fase 1: Como jugadores tenemos una unidad, y luchamos contra 1 enemigo

namespace Leccion13_Ejemplo_POO
{
    class Unidad
    {
        public String nombre;
        public int vida;
        public int ataque;

        // Constructores: SOn métodos especiales para construir, No devuelven nada concreto, sólo un objeto del tipo de la propia clase
        // Constructor por defecto:
        public Unidad()
        {
            this.nombre = "Unidad sin nombre";
            this.vida = 100;
            this.ataque = 10;
        }
        // Constructor con parámetros
        public Unidad(String nombre, int nuevaVida, int ataque)
        {
            this.nombre = nombre;
            this.vida = nuevaVida;
            this.ataque = ataque;
        }
        // Crear un constructor donde la vida por defecto se ponga a 100
        public Unidad(String nombre, int ataque)
        {
            this.nombre = nombre;
            this.vida = 100;
            this.ataque = ataque;
        }
        public String EnTexto()
        {
            return "Nombre :" + this.nombre + ", Vida/Ataque: " + this.vida + "/" + this.ataque;
        }
        public void Mostrar(String tipoUnidad)
        {
            Console.WriteLine("Unidad " + tipoUnidad + " :" + this.nombre);
            Console.WriteLine("Vida/Ataque: " + this.vida + "/" + this.ataque);
        }
        // Crear un método RecibirAtaque(), que quite X vida (parámetro) a la vida del objeto (variable miembro)
        public void RecibirAtaque(int x_ataque)
        {
            this.vida = this.vida - x_ataque;
        }
        public void Atacar(Unidad unidRecibe)
        {
            unidRecibe.vida = unidRecibe.vida - this.ataque;
        }
    }
    class Juego
    {
        public Unidad jugador = null;
        public Unidad enemigo = null;

        public void Inicializar()
        {
            this.enemigo = new Unidad("Ctchulu", 50, 10);
            Unidad otroEnem = new Unidad("Furia", 18);
            // Crear otro enemigo, cuya vida sea 100, con otro nombre y ataque. DEPURAR para COMPROBAR
            this.jugador = new Unidad();
            Console.WriteLine("Dí tu nombre:");
            this.jugador.nombre = Console.ReadLine();
            Console.WriteLine("Indica tu ataque:");
            this.jugador.ataque = int.Parse( Console.ReadLine());
            this.jugador.vida = 30;
        }
        public void MostrarUnidades()
        {
            jugador.Mostrar("JUGADOR");
            Console.WriteLine(jugador.EnTexto());
            enemigo.Mostrar("ENEMIGO");
            Console.WriteLine(enemigo.EnTexto());
        }
        // Programar un método: ComenzarAtaques(), que haga que enemigo ataque a jugador, y jugador a enemigo.

        public void ComenzarAtaques()
        {
            this.jugador.RecibirAtaque(this.enemigo.ataque);  // Enemigo ataca a jugador
            this.enemigo.RecibirAtaque(this.jugador.ataque);  // Viceversa
        }
        public void ContinuarAtaques()
        {
            enemigo.Atacar(jugador);
            jugador.Atacar(enemigo);

        }
    }
    // La clase Program la hacemos static para que NO se pueda instaciar ni usar como objeto.
    static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Juego!");
            Juego miJuego = new Juego();
            miJuego.Inicializar();
            miJuego.MostrarUnidades();
            miJuego.ContinuarAtaques();
        }
    }
}
