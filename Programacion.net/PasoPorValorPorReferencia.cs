using System;

public class PasoPorValorPorReferencia
{
	static void Main ()
	
	{
		
		Console.WriteLine("Paso por valor:");
		int y = 10;
		CambiarVariablePorReferencia(ref y);
		Console.WriteLine("Y = " + y);
		
		
		//Ejemplo de paso por valor
		// Como puede "cascar" hay que controlar la excepción con try/catch
		try
		{
			Console.WriteLine ("Comienzo de búsqueda de errores");
			string supuestoNumero_1 = "200", supuestoNumero_2 = "LOO";
			int numero_1 = Int32.Parse(supuestoNumero_1);
			int numero_2 = Int32.Parse(supuestoNumero_2);
			Console.WriteLine("Num 1 = " + numero_1 + ", Num 2 = " + numero_2);
		}
		catch(Exception error)
		{
			Console.WriteLine("Error genérico");
			Console.WriteLine(error.Message);
		}
		Console.WriteLine ("Fin de búsqueda de errores");
		
		
	}

	//Pasamos el dato por valor: Se hace una copia en memoria
	// Las variables (la anterior y este parámetro)
	// Representan variables independientes)
		
	static void CambiarVariablePorValor (int x)
	{
	  x=20;
	}
	
	static void CambiarVariablePorReferencia(ref int z)
	{
	  z=20;
	}
}