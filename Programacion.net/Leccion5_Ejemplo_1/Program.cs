﻿using System;

namespace Leccion5_Ejemplo_1
{
    class Program
    {
        static void Main()
        {
            //Sumar();
            //Ejercicio1();
            // Ejercicio2();
           // Ejercicio3();
            Ejercicio4();
        }
        public static void Sumar()
        {
            /*int num1;
            int num2;
            string numeroUno;
            string numeroDos;
            

            Console.WriteLine("Introduzca un número: ");
            numeroUno = Console.ReadLine();
            num1 = int.Parse(numeroUno);

            Console.WriteLine("Introduzca otro número: ");
            numeroDos = Console.ReadLine();
            num2 = int.Parse(numeroDos);

            int Sumatorio = num1 + num2;
            Console.WriteLine("La suma entre los números " + num1 + " y " + num2 + " es igual a " + Sumatorio);

            int producto = num1 * num2;
            Console.WriteLine("El producto entre los números " + num1 + " y " + num2 + " es igual a " + producto);*/
        }

        public static void Ejercicio1()
        {
            // Realizar la carga del lado de un cuadrado, mostrar por pantalla el perímetro del mismo (El perímetro de un cuadrado se calcula multiplicando el valor del lado por cuatro) 

            string ladoCuadrado;
           
            Console.WriteLine("Introduce el lado de tu cuadrado: ");
            ladoCuadrado = Console.ReadLine();
            int numLado = int.Parse(ladoCuadrado);
            int perimetroCuadrado = numLado * 4;

            Console.WriteLine("El  perímetro de tu cuadrado es: " + perimetroCuadrado);

        }

        public static void Ejercicio2()
        {
            // Escribir un programa en el cual se ingresen cuatro números, calcular e informar la suma de los dos primeros y el producto del tercero y el cuarto. 
            string numeroUno;
            string numeroDos;
            string numeroTres;
            string numeroCuatro;

            Console.WriteLine("Introduce el primer número: ");
            numeroUno = Console.ReadLine();
           int num1 = int.Parse(numeroUno);

            Console.WriteLine("Introduce el segundo número: ");
            numeroDos = Console.ReadLine();
            int num2 = int.Parse(numeroDos);

            Console.WriteLine("Introduce el tercer número: ");
            numeroTres = Console.ReadLine();
            int num3 = int.Parse(numeroTres);

            Console.WriteLine("Introduce el cuarto número: ");
            numeroCuatro = Console.ReadLine();
            int num4 = int.Parse(numeroCuatro);

            int sumatorio = num1 + num2;
            int producto = num3 * num4;
            Console.WriteLine("La suma de los dos primeros números: " + num1 + " y " + num2 + " es " + sumatorio);
            Console.WriteLine("El producto de los dos segundos números: " + num3 + " y " + num4 + " es " + producto);

        }

        public static void Ejercicio3()
        {
            //Realizar un programa que lea cuatro valores numéricos e informar su suma y promedio. 
            string numeroUno;
            string numeroDos;
            string numeroTres;
            string numeroCuatro;

            Console.WriteLine("Introduce el primer número: ");
            numeroUno = Console.ReadLine();
            float num1 = int.Parse(numeroUno);

            Console.WriteLine("Introduce el segundo número: ");
            numeroDos = Console.ReadLine();
            float num2 = int.Parse(numeroDos);

            Console.WriteLine("Introduce el tercer número: ");
            numeroTres = Console.ReadLine();
            float num3 = int.Parse(numeroTres);

            Console.WriteLine("Introduce el cuarto número: ");
            numeroCuatro = Console.ReadLine();
            float num4 = int.Parse(numeroCuatro);

            float sumatorio = num1 + num2 + num3 + num4;
            float media = (sumatorio / 4);

            Console.WriteLine("La suma de los cuatro números es: "  + sumatorio + " y  su promedio es: " + media);
        }

        public static void Ejercicio4()
        {
            // Se debe desarrollar un programa que pida el ingreso del precio de un artículo y la cantidad que lleva el cliente.Mostrar lo que debe abonar el comprador.

            string articulo = "cuaderno de anillas";
            string cantidad;
            string precio;

            Console.WriteLine("Introduzca el precio de " + articulo);
            precio = Console.ReadLine();
            float numPrecio = float.Parse(precio);

            Console.WriteLine("Inserte la cantidad que desee comprar: ");
            cantidad = Console.ReadLine();
            float numCantidad = float.Parse(cantidad);
            float total = numCantidad * numPrecio;

            Console.WriteLine("La factura del producto es: " + numCantidad + " " + articulo + " " + numPrecio + " euros cada artículo. El precio total es " + total + " Euros.");

        }
    }
}
