﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esctructuras
{
    public struct Jugador 
    {
        public string nombre;
        public int edad;
        public bool haPagado; //True ha pagado, false free to play
        public float vida;
		public static float vidaMax = 1000f;

        // Constructor: Función especial que sirve para inicializar los datos de una estructura del tirón. 
        //En las estructuras es obligatorio inicializar todos los campos (propiedades, variables miembro)

        public Jugador (string n, int edad, bool haPagado, float vida) 
        {
            nombre = n;
            this.edad = edad;
            this.haPagado = haPagado;
            this.vida = vida;
			if (this.vida >= Jugador.vidaMax){this.vida = vidaMax; }
        } 

        public void Mostrar()
        {
            Console.WriteLine("Jugador: " + this.nombre + " edad = " + this.edad + (haPagado ? " VIP" : " Free To Play"));
        }

    }

    //Estructura enemigo: nombre, ataque(float)

    public struct Enemigo 
    {
        public string nombre;
        public float ataque;

        public Enemigo(string n, float ataque) 
        {
            nombre = n;
            this.ataque = ataque;
        }

       public void AtaqueEnemigo(string n, float ataque, float vida)
        {
            Console.WriteLine("Enemigo " + this.nombre + " ataca a " + jug.nombre + " quedándose con " + (jug.vida - ataque) + " puntos de vida");

        }

        public void PlantillaQuitarVida(ref float quitaVida) 
        {
            quitaVida = Jugador.vida - Enemigo.ataque;
        }


        public void AtaqueEnemigoATodos(Jugador jug, Jugador jug2, Jugador jug3)
        {
            Console.WriteLine("Enemigo: " + this.nombre + " ataca a " + jug.nombre + " quedándose con " + (jug.vida - ataque) + " puntos de vida." + "Enemigo: " + this.nombre + " ataca a " + jug2.nombre + " quedándose con " + (jug2.vida - ataque) + " puntos de vida." + "Enemigo: " + this.nombre + " ataca a " + jug3.nombre + " quedándose con " + (jug3.vida - ataque) + " puntos de vida.");
        }

        public void ataqueAVarios(Jugador[] Jugadores) 
        {
        for(int i = 0; i < Jugadores.Length; i++) 
            {
                Jugadores[i].vida = Jugadores[i].vida - this.ataque;
                Jugadores[i].Mostrar();
            }
        }

        public void Mostrar()
        {
            Console.WriteLine("Enemigo: " + this.nombre + " Ataque = " + this.ataque);
        }


    }
    class Program
    {
        static void Main()
        {
            Jugador jug = new Jugador("Fulano", 30, false, 15.5f);
            /*jug.nombre = "Fulano";
            jug.edad = 30;
            jug.haPagado = false;*/
            jug.Mostrar();
            //MostrarPersona(jug.nombre, jug.edad, jug.haPagado);

            Jugador jug2 = new Jugador("Fulanita", 32, true, 10.2f);
            /* jug2.nombre = "Fulanita";
             jug2.edad = 32;
             jug2.haPagado = true;*/
            jug2.Mostrar();
            //MostrarPersona(jug2.nombre, jug2.edad, jug2.haPagado);

            Jugador jug3 = new Jugador("Francisquete", 45, true, 9.9f);
            /* jug3.nombre = "Francisquete";
             jug3.edad = 45;
             jug3.haPagado = true;*/
            jug3.Mostrar();
            //MostrarPersona(jug3.nombre, jug3.edad, jug3.haPagado);

            Enemigo enem = new Enemigo("Molbol", 3.2f);
            Enemigo enem2 = new Enemigo("Bicho bola", 1.4f);
           /* enem.nombre = "Molbol";
            enem.ataque = 3.2f;*/
            enem.Mostrar();
            //MostratEnemigo(enem.nombre, enem.ataque);

            enem.AtaqueEnemigo(jug);
            enem2.AtaqueEnemigoATodos(jug, jug2,jug3);
            enem2.ataqueAVarios(new Jugador[] { jug, jug2, jug3 });
            jug.Mostrar();
            jug2.Mostrar();
            jug3.Mostrar();


        }
        // 1 Crear otro enemigo.
        // 2 Añadir campo vida de tipo float al jugador
        // 3 Hacer un método ataque en enemigo que reciba un jugador como parámetro, y que le quite vida
        // 4 Usarlos: enem_1 ataca al jug, y enem_2 ataca a todos los jugadores
       
    }
}
