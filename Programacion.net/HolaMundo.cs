// Al principio se ponen las importaciones

using System; //Usar las librerías-funcionalidades del sistema

public class HolaMundo // Crear una clase pública

{	
	 static void Main() {			// Creamos un método que le diga a la consola que escriba
		
		Console.Beep();
		{
		Console.WriteLine("¡Hola Guille!");
		Console.WriteLine("¿Qué tal?");
		Console.ReadKey();
		}
		Console.WriteLine("Fenomenal");
		Console.Beep();
		
		// TIPOS DE DATOS
		// Así declaramos una variable (esta es tipo nombre)
		byte unNumero; // byte de 0 a 255
		
		unNumero = 10;
		Console.WriteLine("El byte es " + unNumero);
		
		char unCaracter; // Se refiere a un caracter o bien 1 o 2 bytes
		unCaracter = 'A'; // Comillas simples para caracteres('xxxxx')
							// Comillas dobles para textos de varios caracteres ("xxxx")
		Console.WriteLine("El caracter es " + unCaracter);
						// Un caracter también es un número como todo en un ordenador
						
		unCaracter = (char) (65 + 4); // Para hacer la conversión hay que hacer lo que se denomina "casting"  
								// "Casting" es convertir a otra cosa
								
								
		Console.WriteLine("El carácter es " + unCaracter);
		
		
		
		
	}
	
	
	

}