﻿using System;

namespace Leccion_8
{
    class Program
    {
        static void Main()
        {
            // Ejercicio1();
            //Ejercicio2();
            //Ejercicio3();
            //Ejercicio4();
            //Ejercicio5();
            //Ejercicio6();
            Ejercicio7();
        }
        public static void Ejercicio1()
        {
            // Realizar un programa que pida cargar una fecha cualquiera, luego verificar si dicha fecha corresponde a Navidad. 

            string dia;
            Console.WriteLine("Insertar día (formato dd): ");
            dia = Console.ReadLine();
            int numDia = int.Parse(dia);

            string mes;
            Console.WriteLine("Insertar mes (formato mm): ");
            mes = Console.ReadLine();
            int numMes = int.Parse(mes);

            string year;
            Console.WriteLine("Insertar año (formato aaaa): ");
            year = Console.ReadLine();
            int numYear = int.Parse(year);

            if (numDia < 1 || numDia > 31 ) 
            { Console.WriteLine("Formato de día no válido. Inserte un valor entre 1 y 31.");
                if (numMes < 1 || numMes > 13) 
                {
                    Console.WriteLine("Formato de mes no válido. Inserte un valor entre 1 y 12.");
                    if (numYear < 0 || numYear > 3000) 
                    { 
                        Console.WriteLine("Formato de año no válido. Inserte un valor entre 1 y 3000."); 
                    }
                }
            }
            
            else if (numDia > 1 && numDia < 31 && numMes > 1 && numMes < 13 && numYear > 0 && numYear < 3000)
            {
                Console.WriteLine("La fecha introducida es el día " + numDia + " del mes " + numMes + " del año " + numYear);
                if (numDia == 24 && numMes == 12) { Console.WriteLine("¡¡Feliz navidad!!"); }
            }

        }
        public static void Ejercicio2()
        {
            // Se ingresan tres valores por teclado, si todos son iguales se imprime la suma del primero con el segundo y a este resultado se lo multiplica por el tercero. 

            string valorUno;
            string valorDos;
            string valorTres;

            Console.WriteLine("Introduce el primer valor: ");
            valorUno = Console.ReadLine();
            int valor1 = int.Parse(valorUno);

            Console.WriteLine("Introduce el segundo valor: ");
            valorDos = Console.ReadLine();
            int valor2 = int.Parse(valorDos);

            Console.WriteLine("Introduce el tercer valor: ");
            valorTres = Console.ReadLine();
            int valor3 = int.Parse(valorTres);

            if(valor1 == valor2 && valor1 == valor3)
            {
                int sumatorio = valor1 + valor2;
                int multiplos = sumatorio * valor3;
                Console.WriteLine("La suma del valor 1 (" + valor1 + ") y el valor 2 (" + valor2 + ") es igual a " + sumatorio + ". Y al resultado le multiplicamos el valor 3 (" + valor3 + "). El resultado de esta operación es " + multiplos);
            }

        }
        public static void Ejercicio3()
        {
            //Se ingresan por teclado tres números, si todos los valores ingresados son menores a 10, imprimir en pantalla la leyenda "Todos los números son menores a diez". 
            string numeroUno;
            string numeroDos;
            string numeroTres;

            Console.WriteLine("Introduce el primer número: ");
            numeroUno = Console.ReadLine();
            int num1 = int.Parse(numeroUno);

            Console.WriteLine("Introduce el segundo número: ");
            numeroDos = Console.ReadLine();
            int num2 = int.Parse(numeroDos);

            Console.WriteLine("Introduce el tercer número: ");
            numeroTres = Console.ReadLine();
            int num3 = int.Parse(numeroTres);

            if(num1 < 10 && num2 < 10 && num3 < 10) { Console.WriteLine("Todos los números son menores a diez: el primer número es " + num1 + ", el segundo número es " + num2 + " y el tercer número es " + num3); }
        }
        public static void Ejercicio4()
        {
            // Se ingresan por teclado tres números, si al menos uno de los valores ingresados es menor a 10, imprimir en pantalla la leyenda "Alguno de los números es menor a diez". 
            string numeroUno;
            string numeroDos;
            string numeroTres;

            Console.WriteLine("Introduce el primer número: ");
            numeroUno = Console.ReadLine();
            int num1 = int.Parse(numeroUno);

            Console.WriteLine("Introduce el segundo número: ");
            numeroDos = Console.ReadLine();
            int num2 = int.Parse(numeroDos);

            Console.WriteLine("Introduce el tercer número: ");
            numeroTres = Console.ReadLine();
            int num3 = int.Parse(numeroTres);

            if (num1 < 10 || num2 < 10 || num3 < 10) { Console.WriteLine("Alguno de los números es menor a diez: el primer número es " + num1 + ", el segundo número es " + num2 + " y el tercer número es " + num3); }
        }
        public static void Ejercicio5()
        {
            /* Escribir un programa que pida ingresar la coordenada de un punto en el plano, es decir dos valores enteros x e y (distintos a cero).
            Posteriormente imprimir en pantalla en que cuadrante se ubica dicho punto. (1º Cuadrante si x > 0 Y y > 0 , 2º Cuadrante: x < 0 Y y > 0, etc.) */

            string componenteX;
            string componenteY;

            Console.WriteLine("Introduce la componente X de la coordenada: ");
            componenteX = Console.ReadLine();
            int numComponenteX = int.Parse(componenteX);

            Console.WriteLine("Introduce la componente Y de la coordenada: ");
            componenteY = Console.ReadLine();
            int numComponenteY = int.Parse(componenteY);

            if(numComponenteX > 0 && numComponenteY > 0) 
            { 
                Console.WriteLine("La coordenada se encuentra en el 1º cuadrante. El punto en el espacio es (" + numComponenteX + ", " + numComponenteY + ")."); 
            }
            else if (numComponenteX < 0 && numComponenteY > 0)
            {
                Console.WriteLine("La coordenada se encuentra en el 2º cuadrante. El punto en el espacio es (" + numComponenteX + ", " + numComponenteY + ").");
            }
            else if (numComponenteX < 0 && numComponenteY < 0)
            {
                Console.WriteLine("La coordenada se encuentra en el 3º cuadrante. El punto en el espacio es (" + numComponenteX + ", " + numComponenteY + ").");
            }
            else if (numComponenteX > 0 && numComponenteY < 0)
            {
                Console.WriteLine("La coordenada se encuentra en el 4º cuadrante. El punto en el espacio es (" + numComponenteX + ", " + numComponenteY + ").");
            }
        }
        public static void Ejercicio6()
        {
            /*De un operario se conoce su sueldo y los años de antigüedad. Se pide confeccionar un programa que lea los datos de entrada e informe:
            a) Si el sueldo es inferior a 500 y su antigüedad es igual o superior a 10 años, otorgarle un aumento del 20 %, mostrar el sueldo a pagar.
            b)Si el sueldo es inferior a 500 pero su antigüedad es menor a 10 años, otorgarle un aumento de 5 %.
            c) Si el sueldo es mayor o igual a 500 mostrar el sueldo en pantalla sin cambios. */

            string sueldo;
            string antiguedad;

            Console.WriteLine("Introduzca el sueldo del operario: ");
            sueldo = Console.ReadLine();
            float numSueldo = float.Parse(sueldo);

            Console.WriteLine("Introduzca la antigüedad del operario: ");
            antiguedad = Console.ReadLine();
            float numAntiguedad = float.Parse(antiguedad);

            if(numSueldo < 500 && numAntiguedad >= 10)
            {
                float sueldoAumentado = numSueldo * 1.20f;
                Console.WriteLine("El sueldo a pagar con el aumento del 20% es de " + sueldoAumentado + " euros.");
            }
            else if(numSueldo < 500 && numAntiguedad < 10)
            {
                float sueldoAumentado = numSueldo * 1.05f;
                Console.WriteLine("El sueldo a pagar con el aumento del 5% es de " + sueldoAumentado + " euros.");
            }
            else if(numSueldo >= 500)
            {
                Console.WriteLine("El sueldo a pagar es de " + sueldo + " euros.");
            }

        }
        public static void Ejercicio7() 
        {
            //Escribir un programa en el cual: dada una lista de tres valores numéricos distintos se calcule e informe su rango de variación (debe mostrar el mayor y el menor de ellos) 

            string valorUno;
            string valorDos;
            string valorTres;
            int valorMax = 0;
            int valorMin = 0;

            Console.WriteLine("Introduce el primer número: ");
            valorUno = Console.ReadLine();
            int valor1 = int.Parse(valorUno);

            Console.WriteLine("Introduce el segundo número: ");
            valorDos = Console.ReadLine();
            int valor2 = int.Parse(valorDos);

            Console.WriteLine("Introduce el tercer número: ");
            valorTres = Console.ReadLine();
            int valor3 = int.Parse(valorTres);

            if (valor1 > valor2 && valor1 > valor3)
            { valorMax = valor1; }

            else if (valor2 > valor1 && valor2 > valor3)
            { valorMax = valor2; }

            else 
            { valorMax = valor3; }

            Console.WriteLine("El valor máximo de los tres valores es " + valorMax);

            if (valor1 < valor2 && valor1 < valor3)
            { valorMin = valor1; }

            else if (valor2 < valor1 && valor2 < valor3)
            { valorMin = valor2; }

            else
            { valorMin = valor3; }

            Console.WriteLine("El valor mínimo de los tres valores es " + valorMin);

            int rangoVariacion = valorMax - valorMin;
            Console.WriteLine("El rango de variación entre le valor máximo y el mínimo es: " + rangoVariacion);
        }

    }
}
