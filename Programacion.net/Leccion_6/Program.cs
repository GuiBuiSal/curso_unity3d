﻿using System;

namespace Leccion_6
{
    class Program
    {
        static void Main()
        {
            //Ejercicio1();
            // Ejercicio2();
            Ejercicio3();
        }

        public static void Ejercicio1()
        {
            //Realizar un programa que lea por teclado dos números, si el primero es mayor al segundo informar su suma y diferencia, en caso contrario informar el producto y la división del primero respecto al segundo.

            string cifraUno;
            string cifraDos;

            Console.WriteLine("Introduce un número: ");
            cifraUno = Console.ReadLine();
            float cifra1 = float.Parse(cifraUno);

            Console.WriteLine("Introduce otro número: ");
            cifraDos = Console.ReadLine();
            float cifra2 = float.Parse(cifraDos);
            float sumatorio = cifra1 + cifra2;
            float restatorio = cifra1 - cifra2;
            float producto = cifra1 * cifra2;
            float division = cifra1 / cifra2;

            if (cifra1 > cifra2)
            {
                Console.WriteLine("La suma entre los dos números es: " + sumatorio + " y la resta entre los números es " + restatorio);
            }
            else
            { Console.WriteLine("El producto entre los dos números es: " + producto + " y la división entre los números es " + division); }           
             
        }

        public static void Ejercicio2() 
        {
            // Se ingresan tres notas de un alumno, si el promedio es mayor o igual a siete mostrar un mensaje "Promocionado". 

            string notaUno;
            string notaDos;
            string notaTres;
            Console.WriteLine("Introduzca las nota del Alumno: ");
            Console.WriteLine("Nota 1: ");
            notaUno = Console.ReadLine();
            float nota1 = float.Parse(notaUno);

            Console.WriteLine("Nota 2: ");
            notaDos = Console.ReadLine();
            float nota2 = float.Parse(notaDos);

            Console.WriteLine("Nota 3: ");
            notaTres = Console.ReadLine();
            float nota3 = float.Parse(notaTres);

            float media = (nota1 + nota2 + nota3) / 3;

            if (media >= 7)
            {
                Console.WriteLine("La media del alumno es un " + media + ". Enhorabuena ¡¡has promocionado!!");
            }
            else { Console.WriteLine("La media del alumno es un " + media + ". Lo sentimos mucho NO has promocionado"); }
        }
        public static void Ejercicio3()
        {
            // Se ingresa por teclado un número positivo de uno o dos dígitos (1..99) mostrar un mensaje indicando si el número tiene uno o dos dígitos.
            // (Tener en cuenta que condición debe cumplirse para tener dos dígitos, un número entero) 
            string numero;
            Console.WriteLine("Introduzca un número de uno o dos dígitos: ");
            numero = Console.ReadLine();
            int num1 = int.Parse(numero);
            if(num1 < 10) { Console.WriteLine("El número tiene un dígito"); }
            else { Console.WriteLine("El número tiene dos dígitos"); }
        }
    }

}
