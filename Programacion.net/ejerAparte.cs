using System;

public class EjerciciosArrays
{
	public static void Main ()
	{
		float[] ataques = {3.2f,1.7f, 2.4f,5.0f,7.1f, 4.8f};
		bool[] enmCerca = {false, true, true, false, true, true};

		for (int i = 0; i< ataques.Length; i +=1)
		{
			if (enmCerca[i] == false) 
			{
				Console.WriteLine("Enemigo" + enmCerca[i] + ataques [i] );
			}
		}
		
	}
	// Ejercicio 2: Que devuelva un array con textos con la info de los NO cercanos:
	// El resultado es un array: {"Enemigo 0: 3.2", "Enemigo 3: 5.0f"}

	/*public static float ArrayEnemigosLejanos (float[] ataques, bool[] enmCerca)
	{
		
		for (int i = 0; i< ataques.Length; i +=1)
		{
			if (enmCerca[i] == false) 
			{
				Console.WriteLine("Enemigo" + enmCerca[i] + ataques [i] );
			}
		}
	} */
}