using System;

public class ConversionesDatos
{
	public static void Main() 
	{
		// Convertir un número en texto y float en double
		// Conversiones implícitas
		int edad = 40;
		string resultado = "" + edad;
		resultado = resultado + " era un número entero y ahora es un texto";
		Console.WriteLine(resultado);
		double numDecimal = 6.4545f;
		resultado = resultado + ", el numDecimal = " + numDecimal;
		Console.WriteLine(resultado);
	
		// Conversiones explícitas:
		float otroDecimal = (float)1.2345677891233;
		resultado = resultado + ", otroDecimal = " + otroDecimal;
		Console.WriteLine ( resultado );
		
		// Conversiones de texto a número
		int unEntero = Int32.Parse("3434");
		resultado = resultado + " unEntero = " + unEntero;
		Console.WriteLine( resultado );
		
		//string numA = "15";
		//string numB = "7";
		// Haz que el programa clacule la suma y muestre el resultado
	
		int numA = Int32.Parse("15");
		int numB = Int32.Parse("7");
		int resultadoSuma = numA + numB;
		Console.WriteLine( "El resultado es: " + resultadoSuma );
	}
}